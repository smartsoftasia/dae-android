package pattararittigul.sasin.unesco_android.util

import junit.framework.Assert
import org.junit.Test
import pattararittigul.sasin.unesco_android.extension.asDate
import pattararittigul.sasin.unesco_android.extension.toShortStyle

class NumberFormatUtilTest {


    @Test
    fun convertToShortStyle1_True() {
        val assert = "1k"
        Assert.assertEquals(assert, 1000.toShortStyle())
    }

    @Test
    fun convertToShortStyle2_True() {
        val assert = "1m"
        Assert.assertEquals(assert, 1000000.toShortStyle())
    }


    @Test
    fun convertToShortStyle3_True() {
        val assert = "1k"
        Assert.assertEquals(assert, 1100.toShortStyle())
    }

    @Test
    fun convertToShortStyle4_True() {
        val assert = "1m"
        Assert.assertEquals(assert, 1200000.toShortStyle())
    }

    @Test
    fun convertToShortStyle5_True() {
        val assert = "2k"
        Assert.assertEquals(assert, 1800.toShortStyle())
    }

    @Test
    fun convertToShortStyle6_True() {
        val assert = "2m"
        Assert.assertEquals(assert, 1509000.toShortStyle())
    }


    @Test
    fun convertToShortStyle7_True() {
        val assert = "933"
        Assert.assertEquals(assert, 933.toShortStyle())
    }

    @Test
    fun convertToShortStyle8_True() {
        val assert = "-123"
        Assert.assertEquals(assert, (-123).toShortStyle())
    }

    @Test
    fun convertDateToDayOfWeek_Saturday() {
        val assert = "Saturday"
        Assert.assertEquals(assert, (1572699628000L.asDate("EEEE")))
    }

    @Test
    fun convertDateToDayOfWeek_Thursday() {
        val assert = "Thursday"
        Assert.assertEquals(assert, (1573131628000L.asDate("EEEE")))
    }
}