package pattararittigul.sasin.unesco_android.component

import android.view.View
import androidx.viewpager.widget.ViewPager


class CoverFlowPageTransformer(isZoomEnable: Boolean = false) : ViewPager.PageTransformer {

    private var minScale = 1f
    private val minAlpha = 0.85f

    init {
        minScale = if (isZoomEnable) 0.85f else 1f
    }

    override fun transformPage(view: View, position: Float) {
        val pageWidth = view.width
        val pageHeight = view.height
        var verticalMargin = pageHeight * (1 - minScale) / 2
        var horizontalMargin = pageWidth * (1 - minScale) / 2
        view.scaleX = minScale
        view.scaleY = minScale
        when {
            position < -1 -> {
                view.alpha = minAlpha
                view.translationX = horizontalMargin - verticalMargin / 2
            }
            position <= 1 -> {
                val scaleFactor = minScale.coerceAtLeast(1 - Math.abs(position))
                verticalMargin = pageHeight * (1 - scaleFactor) / 2
                horizontalMargin = pageWidth * (1 - scaleFactor) / 2
                if (position < 0) view.translationX =
                    horizontalMargin - verticalMargin / 2 else view.translationX =
                    -horizontalMargin + verticalMargin / 2
                view.scaleX = scaleFactor
                view.scaleY = scaleFactor
                view.alpha = minAlpha + (scaleFactor - minScale) / (1 - minScale) * (1 - minAlpha)
            }
            else -> {
                view.alpha = minAlpha
                view.translationX = -horizontalMargin + verticalMargin / 2
            }
        }
    }
}