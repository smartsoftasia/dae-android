package pattararittigul.sasin.unesco_android.extension

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


fun Fragment.share(
    image: Uri? = Uri.EMPTY,
    title: String,
    content: String,
    type: String = "text/plain"
) {
    (requireActivity() as AppCompatActivity).share(image, title, content, type)
}

fun AppCompatActivity.share(
    image: Uri? = null,
    title: String = "",
    content: String,
    type: String = "*/*"
) {
    val shareIntent = Intent()
    shareIntent.apply {
        action = Intent.ACTION_SEND
        setType(type)
        putExtra(Intent.EXTRA_TEXT, content)
        image?.let { putExtra(Intent.EXTRA_STREAM, it) }
        putExtra(Intent.EXTRA_TITLE, title)
        addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
    }.also {
        startActivity(Intent.createChooser(shareIntent, "send"))
    }
}

fun Fragment.shareTo(
    image: Uri? = null,
    title: String = "",
    content: String,
    type: String = "*/*",
    sharedTo: String
) {
    (requireActivity() as AppCompatActivity).shareTo(image, title, content, type, sharedTo)
}

fun AppCompatActivity.shareTo(
    image: Uri? = null,
    title: String = "",
    content: String,
    type: String = "*/*",
    sharedTo: String
) {
    val shareIntent = Intent(this.packageManager.getLaunchIntentForPackage(sharedTo))
    shareIntent.apply {
        action = Intent.ACTION_SEND
        setType(type)
        putExtra(Intent.EXTRA_TEXT, content)
        image?.let { putExtra(Intent.EXTRA_STREAM, it) }
        putExtra(Intent.EXTRA_TITLE, title)
    }.also {
        startActivity(it)
    }
}