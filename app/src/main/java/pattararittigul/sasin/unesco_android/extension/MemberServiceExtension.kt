package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.MemberApi
import pattararittigul.sasin.unesco_android.model.BaseResponse
import pattararittigul.sasin.unesco_android.model.ProfileModel
import pattararittigul.sasin.unesco_android.service.RetrofitService
import java.io.File

private fun memberService(): MemberApi = RetrofitService.retrofit().create(MemberApi::class.java)


fun executeRegister(
    firstName: String,
    lastName: String,
    hometown: String?,
    email: String,
    password: String
): Observable<BaseResponse> = memberService().registerAccount(
    firstName.toRequestBody(),
    lastName.toRequestBody(),
    hometown?.toRequestBody(),
    email.toRequestBody(),
    password.toRequestBody()
).execute()

fun executeLogin(
    email: String,
    password: String
): Observable<ProfileModel> =
    memberService().loginAccount(
        email.toRequestBody(),
        password.toRequestBody()
    ).execute()

fun executeUpdateProfile(
    id: String,
    firstName: String?,
    lastName: String?,
    hometown: String?,
    profile: File?,
    appToken: String?
): Observable<BaseResponse> = memberService().updateAccount(
    id = id.toRequestBody(),
    firstName = firstName?.toRequestBody(),
    lastName = lastName?.toRequestBody(),
    profile = profile?.let { filePathToMultipartRequestBody(it, "image") },
    hometown = hometown?.toRequestBody(),
    appToken = appToken?.toRequestBody()
).execute()


fun executeGetDetail(id: String): Observable<ProfileModel> =
    memberService().getAccountDetail(id.toRequestBody()).execute()


fun executeLoginFacebook(facebookId: String, token: String): Observable<ProfileModel> =
    memberService().loginFacebook(facebookId.toRequestBody(), token.toRequestBody()).execute()

fun executeResetPassword(email: String): Observable<BaseResponse> {
    return memberService().resetPassword(email.toRequestBody()).execute()
}

























