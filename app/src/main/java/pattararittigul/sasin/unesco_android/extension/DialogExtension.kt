package pattararittigul.sasin.unesco_android.extension

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment


fun Fragment.errorAlertDialog(message: String = getString(pattararittigul.sasin.unesco_android.R.string.email_pattern_error_desc)) {
    AlertDialog.Builder(requireContext())
        .setTitle(getString(pattararittigul.sasin.unesco_android.R.string.default_error_title_dialog))
        .setMessage(message)
        .setPositiveButton(getString(pattararittigul.sasin.unesco_android.R.string.dialog_dismiss_button)) { dialog, _ ->
            dialog.dismiss()
        }
        .also { it.show() }
}

fun Fragment.displayDialog(
    @StringRes firstButton: Int,
    eventFirst: DialogInterface.OnClickListener,
    @StringRes secondButton: Int,
    eventSecond: DialogInterface.OnClickListener,
    @StringRes thirdButton: Int,
    eventThird: DialogInterface.OnClickListener
) {
    AlertDialog.Builder(requireContext())
        .setPositiveButton(firstButton, eventFirst)
        .setNeutralButton(secondButton, eventSecond)
        .setNeutralButton(thirdButton, eventThird)
        .setCancelable(false)
        .also {
            it.show()
        }
}

inline fun <reified T : Any> Fragment.displayItemDialog(buttons: Pair<Array<String>, Array<(T) -> Unit>>) {
    val (names, events) = buttons
    val builder = androidx.appcompat.app.AlertDialog.Builder(requireContext())
    builder.setItems(names) { dialog, which ->
        when (names[which]) {
            names[0] -> {
                (events[0])
                dialog.dismiss()
            }
            names[1] -> {
                (events[1])
                dialog.dismiss()
            }
            names[2] -> {
                events[2]
                dialog.dismiss()
            }
        }
    }.also { it.show() }
}

fun Fragment.displayDialog(
    title: String = "",
    message: String = "",
    firstButton: String,
    eventFirst: DialogInterface.OnClickListener,
    secondButton: String,
    eventSecond: DialogInterface.OnClickListener,
    cancelable: Boolean = false
) {
    AlertDialog.Builder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setNegativeButton(secondButton, eventSecond)
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Fragment.displayDialog(
    title: Int,
    message: Int,
    firstButton: Int,
    eventFirst: DialogInterface.OnClickListener,
    secondButton: Int,
    eventSecond: DialogInterface.OnClickListener,
    cancelable: Boolean = false
) {
    AlertDialog.Builder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setNegativeButton(secondButton, eventSecond)
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Fragment.displayDialog(
    title: String = "",
    message: String = "",
    firstButton: String,
    eventFirst: DialogInterface.OnClickListener,
    cancelable: Boolean
) {
    AlertDialog.Builder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Fragment.displayDialog(
    @StringRes title: Int,
    @StringRes message: Int,
    @StringRes firstButton: Int,
    eventFirst: DialogInterface.OnClickListener,
    cancelable: Boolean
) {
    AlertDialog.Builder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Fragment.displayErrorDialog(
    title: Int = pattararittigul.sasin.unesco_android.R.string.default_error_title_dialog,
    message: Int = pattararittigul.sasin.unesco_android.R.string.default_error_desc_dialog,
    firstButton: Int = pattararittigul.sasin.unesco_android.R.string.dismiss,
    eventFirst: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
) {
    displayDialog(
        title,
        message,
        firstButton,
        eventFirst,
        false
    )
}

fun Fragment.displayErrorDialog(
    title: Int = pattararittigul.sasin.unesco_android.R.string.default_error_title_dialog,
    message: String,
    firstButton: Int = pattararittigul.sasin.unesco_android.R.string.dismiss,
    eventFirst: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }

) {
    displayDialog(
        getString(title),
        message,
        getString(firstButton),
        eventFirst,
        false
    )
}


fun Fragment.showLoading(): ProgressDialog =
    ProgressDialog.show(
        requireContext(),
        "",
        getString(pattararittigul.sasin.unesco_android.R.string.loading),
        true,
        false
    )

fun ProgressDialog?.hideLoading() = this?.let {
    if (it.isShowing) this.dismiss()
}


fun Context.toast(text: CharSequence): Toast? =
    Toast.makeText(this, text, Toast.LENGTH_SHORT).apply { show() }

fun Fragment.toast(text: CharSequence) = activity?.toast(text)