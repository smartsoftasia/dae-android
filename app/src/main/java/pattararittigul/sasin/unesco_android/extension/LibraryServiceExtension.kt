package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.LibraryApi
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel
import pattararittigul.sasin.unesco_android.service.RetrofitService

private val libraryService = RetrofitService.retrofit().create(LibraryApi::class.java)

fun executeLibraryAll(
    languageId: Int = SharedPreferences.getLanguageId(),
    fileCategory: String = "1"
): Observable<LibraryAllResponseModel> = libraryService.getLibraryAll(
    languageId.toRequestBody(),
    fileCategory.toRequestBody()
).execute()