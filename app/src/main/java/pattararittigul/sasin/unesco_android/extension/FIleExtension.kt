package pattararittigul.sasin.unesco_android.extension

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import pattararittigul.sasin.unesco_android.BuildConfig
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

fun Context.getFileUri(photoFile: File): Uri? {
    return FileProvider.getUriForFile(
        this,
        "${BuildConfig.APPLICATION_ID}.provider",
        photoFile
    )
}


@Throws(IOException::class)
fun Fragment.createImageFile(): File {
    val datePattern = "yyyyMMdd_HHmmss"
    val timeStamp = SimpleDateFormat(datePattern, Locale.US).format(Date())
    val fileName = "JPEG_${timeStamp}"
    val suffix = ".jpg"
    val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    return File.createTempFile(fileName, suffix, storageDir)
}


@Throws(IOException::class)
fun Fragment.getBitmapFromUri(uri: Uri): Bitmap {
    val parcelFileDescriptor = requireContext().contentResolver.openFileDescriptor(uri, "r")
    val fileDescriptor = parcelFileDescriptor?.fileDescriptor
    val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
    parcelFileDescriptor?.close()
    return image
}

@Throws(IOException::class)
fun Context.getBitmapFromUri(uri: Uri): Bitmap {
    val parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r")
    val fileDescriptor = parcelFileDescriptor?.fileDescriptor
    val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
    parcelFileDescriptor?.close()
    return image
}

fun createFolder(folderName: String): File {
    val path = "${Environment.getExternalStorageDirectory().absolutePath}$folderName"
    val folder = File(path)
    if (!folder.exists()) {
        folder.mkdirs()
    }
    return folder
}
