package pattararittigul.sasin.unesco_android.extension

import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import androidx.fragment.app.Fragment
import com.facebook.FacebookSdk
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import java.util.*


fun Fragment.shareToFacebook(imageUri: Uri, content: String?) {
    val shareDialog = ShareDialog(this)
    FacebookSdk.sdkInitialize(requireContext().applicationContext)
    val linkContent = ShareLinkContent.Builder()
        .setQuote(content)
        .setContentUrl(imageUri)
        .build()
    shareDialog.show(linkContent)
}


fun Fragment.shareToLine(message: String?) {
    val intent = Intent()
    intent.action = Intent.ACTION_VIEW
    intent.data = Uri.parse("line://msg/text/$message")
    startActivity(intent)
}

fun Fragment.shareToTwitter(message: String?) {
    val twitterUrl = StringBuilder("https://twitter.com/intent/tweet?text=")
    twitterUrl.append(Uri.encode(message))
    val twitterIntent = Intent(Intent.ACTION_VIEW, Uri.parse(twitterUrl.toString()))
    val matches = requireActivity().packageManager.queryIntentActivities(twitterIntent, 0)
    for (info: ResolveInfo in matches) {
        if (info.activityInfo.packageName.toLowerCase(Locale.ENGLISH).startsWith("com.twitter")) {
            twitterIntent.setPackage(info.activityInfo.packageName)
        }
    }
    requireActivity().startActivity(twitterIntent)
}

fun Fragment.shareToDefault(title: String?, message: String?) {
    val shareIntent = Intent()
    shareIntent.action = Intent.ACTION_SEND
    shareIntent.putExtra(Intent.EXTRA_TITLE, title)
    shareIntent.type = "text/plain"
    shareIntent.putExtra(Intent.EXTRA_TEXT, message)
    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    startActivity(Intent.createChooser(shareIntent, "send"))
}