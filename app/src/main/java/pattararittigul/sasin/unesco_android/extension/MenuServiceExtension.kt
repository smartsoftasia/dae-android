package pattararittigul.sasin.unesco_android.extension

import android.content.ContentValues.TAG
import android.util.Log
import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.MenuApi
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.model.AboutUsResponseModel
import pattararittigul.sasin.unesco_android.model.AllDirectoryResponseModel
import pattararittigul.sasin.unesco_android.model.BaseResponse
import pattararittigul.sasin.unesco_android.model.ContactUsResponseModel
import pattararittigul.sasin.unesco_android.service.RetrofitService
import java.io.File

private fun menuService(): MenuApi = RetrofitService.retrofit().create(MenuApi::class.java)

fun executeAboutUs(languageId: Int = SharedPreferences.getLanguageId()): Observable<AboutUsResponseModel> =
    menuService().aboutusAll(languageId.toRequestBody()).execute()

fun executeContactUs(languageId: Int = SharedPreferences.getLanguageId()): Observable<ContactUsResponseModel> =
    menuService().contactusAll(languageId.toRequestBody()).execute()

fun executeDirectoryAll(
    languageId: Int = SharedPreferences.getLanguageId(),
    id: String = "1"//"${SharedPreferences.getUserDetail().id}"
): Observable<AllDirectoryResponseModel> {
    Log.d(TAG, "")
    return menuService().directoryAll(languageId.toRequestBody(), id.toRequestBody()).execute()
}

fun executeAddDirectory(
    memberId: String = "${SharedPreferences.getUserDetail().id}",
    image: File?,
    name: String?,
    position: String?,
    phone: String?,
    email: String?,
    township: String?
): Observable<BaseResponse> {
    return menuService().addDirectory(
        memberId.toRequestBody(),
        image?.let { filePathToMultipartRequestBody(it, "profile_image") },
        name?.toRequestBody(),
        position?.toRequestBody(),
        phone?.toRequestBody(),
        email?.toRequestBody(),
        township?.toRequestBody()
    ).execute()
}