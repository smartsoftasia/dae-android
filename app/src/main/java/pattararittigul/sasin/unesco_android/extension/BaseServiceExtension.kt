package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File


fun String.toRequestBody(mediaType: String = "text/plain"): RequestBody {
    val type: MediaType? = mediaType.toMediaTypeOrNull()
    return RequestBody.create(type, this)
}

fun filePathToMultipartRequestBody(file: File, param: String): MultipartBody.Part {
    val requestFile = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
    return MultipartBody.Part.createFormData(param, file.name, requestFile)
}

fun Int.toRequestBody(mediaType: String? = "text/plain"): RequestBody {
    val type = mediaType?.toMediaTypeOrNull()
    return RequestBody.create(type, this.toString())
}

inline fun <reified T : Any?> Observable<T>.execute(): Observable<T> {
    return this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

