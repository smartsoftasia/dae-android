package pattararittigul.sasin.unesco_android.extension

import pattararittigul.sasin.unesco_android.enum.DateTimeFormat
import pattararittigul.sasin.unesco_android.util.DateUtil
import kotlin.math.abs
import kotlin.math.roundToInt

fun Long.asTime(format: String = DateTimeFormat.TIME_DEFAULT.pattern): String {
    return asDateTime(format)
}

fun Long.asDate(format: String = DateTimeFormat.DATE_DEFAULT.pattern): String {
    return asDateTime(format)
}

fun Long.asDateTime(format: String = DateTimeFormat.DATE_TIME_SHORT_DEFAULT.pattern): String {
    return DateUtil.formatDateMillisToDateString(this, format) ?: "–"
}

fun Int.toShortStyle(): String = this.toDouble().toShortStyle()

fun Double.toShortStyle(): String {
    return when {
        abs(this / 1000000) >= 1 -> """${((this / 1000000).roundToInt())}m"""
        abs(this / 1000) >= 1 -> """${((this / 1000).roundToInt())}k"""
        else -> "${this.roundToInt()}"
    }
}
