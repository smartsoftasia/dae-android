package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.UploadApi
import pattararittigul.sasin.unesco_android.model.UploadImageResponse
import pattararittigul.sasin.unesco_android.service.RetrofitService
import java.io.File

private fun uploadImageService(): UploadApi =
    RetrofitService.retrofit().create(UploadApi::class.java)

fun executeUploadImage(file: File, param: String = "image"): Observable<UploadImageResponse> {
    return uploadImageService().uploadImage(filePathToMultipartRequestBody(file, param)).execute()
}