package pattararittigul.sasin.unesco_android.extension

import android.app.ProgressDialog
import android.content.Context
import pattararittigul.sasin.unesco_android.R

inline val <reified T : Any> T.TAG get():String = this::class.java.simpleName.toString()

inline val Context.loadingDialog
    get():ProgressDialog = ProgressDialog(this).apply {
        setMessage(context.getString(R.string.please_wait))
    }


