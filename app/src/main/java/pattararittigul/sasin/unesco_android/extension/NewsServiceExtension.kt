package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.NewsApi
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.model.NewsResponseModel
import pattararittigul.sasin.unesco_android.service.RetrofitService
import retrofit2.Response

private fun newsService(): NewsApi = RetrofitService.retrofit().create(NewsApi::class.java)

fun executeAllNews(languageId: Int = SharedPreferences.getLanguageId()): Observable<NewsResponseModel> =
    newsService().getAllNews(languageId.toRequestBody()).execute()

fun executeViewNews(newsId: String): Observable<Response<Void>> =
    newsService().newsView(newsId.toRequestBody()).execute()