package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.EventApi
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel
import pattararittigul.sasin.unesco_android.service.RetrofitService

private fun eventService(): EventApi = RetrofitService.retrofit().create(EventApi::class.java)

fun executeAllEvent(languageId: Int = SharedPreferences.getLanguageId()): Observable<EventCalendarResponseModel> {
    return eventService().getEventAll(languageId.toRequestBody()).execute()
}