package pattararittigul.sasin.unesco_android.extension

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import pattararittigul.sasin.unesco_android.R

fun Fragment.nextActivityAnimate() {
    requireActivity().overridePendingTransition(
        R.anim.slide_in_right,
        R.anim.slide_out_left
    )
}

fun AppCompatActivity.nextActivityAnimate() {
    overridePendingTransition(
        R.anim.slide_in_right,
        R.anim.slide_out_left
    )
}

fun Fragment.previousActivityAnimate() {
    requireActivity().overridePendingTransition(
        R.anim.slide_in_left,
        R.anim.slide_out_right
    )
}

fun AppCompatActivity.previousActivityAnimate() {
    overridePendingTransition(
        R.anim.slide_in_left,
        R.anim.slide_out_right
    )
}

fun Fragment.modalInActivityAnimate() {
    (activity as AppCompatActivity).modalInActivityAnimate()
}

fun AppCompatActivity.modalInActivityAnimate() {
    overridePendingTransition(
        R.anim.slide_in_bottom, android.R.anim.fade_out
    )
}

fun Fragment.modalOutActivityAnimate() {
    (activity as AppCompatActivity).modalOutActivityAnimate()
}

fun AppCompatActivity.modalOutActivityAnimate() {
    overridePendingTransition(
        android.R.anim.fade_in, R.anim.slide_out_bottom
    )
}

fun Fragment.finish() {
    this.requireActivity().finish()
    hideKeyboard()
}