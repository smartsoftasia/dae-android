package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.ThreadApi
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.model.BaseResponse
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel
import pattararittigul.sasin.unesco_android.service.RetrofitService
import retrofit2.Response


private fun threadService() = RetrofitService.retrofit().create(ThreadApi::class.java)

fun executeAllThread(memberId: String = SharedPreferences.getUserDetail().id.toString()): Observable<CommunityStoryResponseModel> =
    threadService().getThreadAll(memberId.toRequestBody()).execute()

fun executeAddStory(
    memberId: String,
    imageUrls: String?,
    title: String,
    detail: String
): Observable<BaseResponse> {
    return threadService().addStory(
        memberId.toRequestBody(),
        imageUrls?.toRequestBody(),
        title.toRequestBody(),
        detail.toRequestBody()
    ).execute()
}

fun executeComment(
    storyId: String,
    memberId: String,
    comment: String
): Observable<BaseResponse> {
    return threadService().addComment(
        storyId.toRequestBody(),
        memberId.toRequestBody(),
        comment.toRequestBody()
    ).execute()
}


fun executeFetchDetail(
    storyId: String,
    memberId: String = SharedPreferences.getUserDetail().id.toString()
): Observable<CommunityStoryResponseModel> =
    threadService().fetchDetail(storyId.toRequestBody(), memberId.toRequestBody()).execute()

fun executeAddLike(
    storyId: String,
    memberId: String = SharedPreferences.getUserDetail().id.toString(),
    isLike: Boolean
): Observable<BaseResponse> =
    threadService().addLike(storyId.toRequestBody(), memberId.toRequestBody(), isLike)
        .execute()


fun executeViewThread(threadId: String): Observable<Response<Void>> =
    threadService().threadView(threadId.toRequestBody()).execute()