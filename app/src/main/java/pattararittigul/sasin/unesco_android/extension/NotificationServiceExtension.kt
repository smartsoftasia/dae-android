package pattararittigul.sasin.unesco_android.extension

import io.reactivex.Observable
import pattararittigul.sasin.unesco_android.api.NotificationApi
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.model.NotificationAllResponseModel
import pattararittigul.sasin.unesco_android.service.RetrofitService

private fun notificationService(): NotificationApi =
    RetrofitService.retrofit().create(NotificationApi::class.java)

fun executeNotificationAll(
    id: String = "${SharedPreferences.getUserDetail().id}",
    languageId: Int = SharedPreferences.getLanguageId()
): Observable<NotificationAllResponseModel> =
    notificationService().getNotificationAll(
        id.toRequestBody(),
        languageId.toRequestBody()
    ).execute()