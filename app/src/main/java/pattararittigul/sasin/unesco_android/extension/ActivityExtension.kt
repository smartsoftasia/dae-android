package pattararittigul.sasin.unesco_android.extension

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Parcelable
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import pattararittigul.sasin.unesco_android.R


fun AppCompatActivity.replaceFragmentNow(@IdRes containerId: Int, fragment: Fragment) {
    hideKeyboard()
    this.supportFragmentManager.transactNow {
        replace(containerId, fragment)
    }
}

fun AppCompatActivity.replaceFragmentWithTag(
    @IdRes containerId: Int, fragment: Fragment,
    tag: String
) {
    hideKeyboard()
    this.supportFragmentManager.transactNow {
        replace(containerId, fragment, tag)
    }
}

fun AppCompatActivity.hideKeyboard() {
    if (currentFocus != null) {
        val inputMethodManager = getSystemService(
            Context
                .INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}

fun Fragment.hideKeyboard() {
    (activity as? AppCompatActivity)?.hideKeyboard()
}

fun AppCompatActivity.replaceFragmentWithAnimation(
    @IdRes containerId: Int, fragment: Fragment,
    name: String = this::class.java.simpleName
) {
    hideKeyboard()
    this.supportFragmentManager
        .beginTransaction()
        .setCustomAnimations(
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )
        .replace(containerId, fragment)
        .addToBackStack(name)
        .commit()
}

fun AppCompatActivity.replaceFragmentWithAnimationNoBackStack(@IdRes containerId: Int, fragment: Fragment) {
    hideKeyboard()
    this.supportFragmentManager
        .beginTransaction().apply {
            setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
                .replace(containerId, fragment)
        }
        .commitNow()
}

fun Fragment.replaceFragmentWithAnimationNoBackStack(@IdRes containerId: Int, fragment: Fragment) {
    hideKeyboard()
    (activity as AppCompatActivity).replaceFragmentWithAnimationNoBackStack(containerId, fragment)
}


fun Fragment.replaceFragmentWithAnimation(
    @IdRes containerId: Int, fragment: Fragment,
    name: String = this::class.java.simpleName
) {
    hideKeyboard()
    (activity as AppCompatActivity).replaceFragmentWithAnimation(containerId, fragment, name)
}

fun Fragment.replaceFragmentNow(@IdRes containerId: Int, fragment: Fragment) {
    hideKeyboard()
    (activity as AppCompatActivity).replaceFragmentNow(containerId, fragment)
}


private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

private inline fun FragmentManager.transactNow(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commitAllowingStateLoss()
}


infix fun AppCompatActivity.setStatusBarColor(@ColorInt color: Int) {
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.statusBarColor = color
}

fun AppCompatActivity.setStatusBarColor(@ColorRes colorIdRes: Int, mTheme: Resources.Theme = theme) {
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.statusBarColor = resources.getColor(colorIdRes, mTheme)
}

fun AppCompatActivity.setKeyboardAlwaysHidden() {
    this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
}


inline fun <reified T : AppCompatActivity?> AppCompatActivity.toActivity() {
    val intent = Intent(this, T::class.java)
    hideKeyboard()
    this.startActivity(intent)
}

inline fun <reified T : AppCompatActivity?> AppCompatActivity.toActivityForResult(requestCode: Int) {
    val intent = Intent(this, T::class.java)
    hideKeyboard()
    this.startActivityForResult(intent, requestCode)
}

inline fun <reified T : AppCompatActivity?> Fragment.toActivityForResult(requestCode: Int) {
    val intent = Intent(requireContext(), T::class.java)
    hideKeyboard()
    this.startActivityForResult(intent, requestCode)
}

inline fun <reified T : AppCompatActivity?> Fragment.toActivity() {
    val intent = Intent(requireContext(), T::class.java)
    hideKeyboard()
    this.startActivity(intent)
}

inline fun <reified T : AppCompatActivity?> AppCompatActivity.toActivityWith(
    key: String,
    parcelable: Parcelable
) {
    val intent = Intent(this, T::class.java)
    intent.putExtra(key, parcelable)
    this.startActivity(intent)
}

inline fun <reified T : AppCompatActivity?> Fragment.toActivityWith(
    key: String,
    parcelable: Parcelable
) {
    val intent = Intent(requireContext(), T::class.java)
    intent.putExtra(key, parcelable)
    this.startActivity(intent)
}

inline fun <reified T : AppCompatActivity?> AppCompatActivity.toActivityToResultWith(
    key: String,
    parcelable: Parcelable,
    requestCode: Int
) {
    val intent = Intent(this, T::class.java)
    intent.putExtra(key, parcelable)
    this.startActivityForResult(intent, requestCode)
}


inline fun <reified T : AppCompatActivity?> Fragment.toActivityToResultWith(
    key: String,
    parcelable: Parcelable,
    requestCode: Int
) {
    val intent = Intent(requireContext(), T::class.java)
    intent.putExtra(key, parcelable)
    this.startActivityForResult(intent, requestCode)
}
