package pattararittigul.sasin.unesco_android.view.main.menu.changelanguage

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import pattararittigul.sasin.unesco_android.extension.previousActivityAnimate
import pattararittigul.sasin.unesco_android.extension.setStatusBarColor
import pattararittigul.sasin.unesco_android.view.BaseActivity
import pattararittigul.sasin.unesco_android.view.main.MainActivity

class ChangeLanguageActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(pattararittigul.sasin.unesco_android.R.layout.change_language_activity)
        setStatusBarColor(
            ContextCompat.getColor(
                this,
                pattararittigul.sasin.unesco_android.R.color.color_primary_variant
            )
        )
        if (savedInstanceState == null) {
            replaceFragment()
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("DIRECT_PAGE", "MENU")
        startActivity(intent)
        previousActivityAnimate()
        finishAffinity()
    }

    private fun replaceFragment() {
        supportFragmentManager.beginTransaction()
            .replace(
                pattararittigul.sasin.unesco_android.R.id.container,
                ChangeLanguageFragment.newInstance()
            )
            .commitAllowingStateLoss()
    }
}
