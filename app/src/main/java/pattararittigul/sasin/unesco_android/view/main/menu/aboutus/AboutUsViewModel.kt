package pattararittigul.sasin.unesco_android.view.main.menu.aboutus

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeAboutUs
import pattararittigul.sasin.unesco_android.model.AboutUsResponseModel

class AboutUsViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val aboutUsData = MutableLiveData<AboutUsResponseModel.Data>()

    fun getAboutUs() {
        executeAboutUs()
            .subscribeBy(
                onNext = {
                    if (it.status) aboutUsData.postValue(it.data)
                    else aboutUsData.postValue(null)
                },
                onError = { aboutUsData.postValue(null) }
            )
            .addTo(compositeDisposable)
    }

    fun aboutUsResult(): LiveData<AboutUsResponseModel.Data> = aboutUsData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }


}

