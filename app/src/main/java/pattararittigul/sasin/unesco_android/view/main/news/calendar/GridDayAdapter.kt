package pattararittigul.sasin.unesco_android.view.main.news.calendar

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel

class GridDayAdapter(private val days: MutableList<EventCalendarResponseModel.Data.CalendarMonth.Day>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_new_day, parent, false)
        return CalendarDayViewHolder(
            view
        )
    }

    override fun getItemCount(): Int = days.size

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (holder is CalendarDayViewHolder) {
            val day = days[position]
            holder.setViewData(day)
        }
    }

}
