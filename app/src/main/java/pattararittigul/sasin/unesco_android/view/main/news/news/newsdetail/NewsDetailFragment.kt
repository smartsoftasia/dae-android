package pattararittigul.sasin.unesco_android.view.main.news.news.newsdetail

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.news_detail_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.*
import pattararittigul.sasin.unesco_android.model.NewsResponseModel
import pattararittigul.sasin.unesco_android.view.main.community.communitydetail.CommunityPagerAdapter


class NewsDetailFragment : Fragment() {

    private lateinit var mView: View
    private lateinit var viewModel: NewsDetailViewModel
    private var newsId: String? = ""

    companion object {
        private const val ARGS_NEWS = "ARGS_NEWS"

        fun newInstance(news: NewsResponseModel.Data.NewsStory): NewsDetailFragment {
            val fragment = NewsDetailFragment()
            val args = Bundle()
            args.putParcelable(ARGS_NEWS, news)
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.news_detail_fragment, container, false)
        val news: NewsResponseModel.Data.NewsStory? = arguments?.getParcelable(ARGS_NEWS)
        news?.let { init(it) }
        return mView
    }

    private fun init(news: NewsResponseModel.Data.NewsStory) {
        newsId = news.news_id.toString()
        news.images?.let { initViewPager(it) }
        mView.toolbar.setNavigationOnClickListener {
            finish()
        }
        mView.toolbar.setOnMenuItemClickListener { item: MenuItem? ->
            when (item?.itemId) {
                R.id.menu_share -> {
                    shareToDefault(news.title, news.description_content)
                    true
                }
                else -> false
            }
        }

        mView.shareFacebookButton.setOnClickListener {
            shareToFacebook(Uri.parse(news.images?.get(0)), news.description_content)
        }

        mView.shareLineButton.setOnClickListener {
            shareToLine(news.description_content)
        }

        mView.shareTwitterButton.setOnClickListener {
            shareToTwitter(news.description_content)
        }





        displayContent(news)
    }

    private fun displayContent(mStory: NewsResponseModel.Data.NewsStory) {
        mView.textTitle.text = mStory.title
        mView.textNewsContent.text = mStory.description_content
        mView.textCountView.text =
            "${mStory.total_viewed_amount?.toShortStyle()} ${getString(R.string.suffix_views)}"
        mView.textDate.text = "${mStory.date_created?.asDate()}"
        mView.textTime.text = "${mStory.date_created?.asTime()}"
    }


    private fun initViewPager(images: MutableList<String>) {
        val pagerAdapter = CommunityPagerAdapter(childFragmentManager, images)
        mView.viewPager.offscreenPageLimit = 2
        mView.viewPager.adapter = pagerAdapter
        mView.pageIndicator.attachViewPager(mView.viewPager)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(NewsDetailViewModel::class.java)
        newsId?.let { viewModel.newsViews(it) }
    }
}
