package pattararittigul.sasin.unesco_android.view.main.news.calendar

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_new_calendar_event.view.*
import pattararittigul.sasin.unesco_android.extension.asDateTime
import pattararittigul.sasin.unesco_android.extension.asTime
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel

class EventViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun setupView(event: EventCalendarResponseModel.Data.CalendarMonth.Day.Event) {
        itemView.textDay.text = (event.event_start?.asDateTime("EEEE") ?: "–").compatWithHtml()
        itemView.textDateEvent.text = (event.event_start?.asDateTime("d") ?: "–").compatWithHtml()
        itemView.textTitleEvent.text = (event.event_name ?: "–").compatWithHtml()
        itemView.textDescEvent.text = (event.event_description ?: "–").compatWithHtml()
        itemView.textDayTimeEvent.text = dateTimeEvent(event).compatWithHtml()
    }

    private fun dateTimeEvent(event: EventCalendarResponseModel.Data.CalendarMonth.Day.Event): String {
        val start = event.event_start?.asTime()
        val end = event.event_end?.asTime()
        return "$start - $end"
    }

    private fun String.compatWithHtml(): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(this)
        }
    }
}
