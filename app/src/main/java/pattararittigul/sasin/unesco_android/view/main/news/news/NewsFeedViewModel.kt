package pattararittigul.sasin.unesco_android.view.main.news.news

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeAllNews
import pattararittigul.sasin.unesco_android.model.NewsResponseModel

class NewsFeedViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val newsData = MutableLiveData<NewsResponseModel.Data>()


    fun fetchNews(input: String = "") {
        executeAllNews()
            .subscribeBy(
                onNext = {
                    if (input.isNotBlank()) {
                        searchResult(input, it.data)
                    } else {
                        newsData.postValue(it.data)
                    }
                },
                onError = {
                    newsData.postValue(null)
                }
            )
            .addTo(compositeDisposable)
    }

    private fun searchResult(input: String, data: NewsResponseModel.Data?) {
        val news = data?.news_stories
        val searchList = news?.filter { it.title?.contains(input, true) == true }?.toMutableList()
        newsData.postValue(searchList?.let { NewsResponseModel.Data(it) })
    }


    fun newsResult(): LiveData<NewsResponseModel.Data> = newsData


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
