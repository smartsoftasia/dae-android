package pattararittigul.sasin.unesco_android.view.main.notification

import android.app.ProgressDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.main_notification_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.hideKeyboard
import pattararittigul.sasin.unesco_android.extension.loadingDialog
import pattararittigul.sasin.unesco_android.model.NotificationModel

class MainNotificationFragment : Fragment() {

    private lateinit var mView: View
    private val adapter = NotificationAdapter(mutableListOf())
    private val loading: ProgressDialog by lazy { requireContext().loadingDialog }

    companion object {
        fun newInstance() = MainNotificationFragment()
    }

    private lateinit var viewModel: MainNotificationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.main_notification_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        mView.recyclerViewNotification.layoutManager = layoutManager
        mView.recyclerViewNotification.setHasFixedSize(true)
        mView.recyclerViewNotification.adapter = adapter

        mView.inputSearch.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    performSearch(mView.inputSearch.text.toString().trim())
                    true
                }
                else -> false
            }
        }
        mView.inputSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    viewModel.getAllNotification()
                    loading.show()
                }
            }
        })
    }


    private fun performSearch(input: String) {
        if (input.isNotBlank()) {
            viewModel.getAllNotification(input)
        } else {
            viewModel.getAllNotification()
        }
        loading.show()
        hideKeyboard()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainNotificationViewModel::class.java)
        viewModel.getAllNotification()
        observeNotificationResult()
        loading.show()
    }

    private fun observeNotificationResult() {
        viewModel.notificationResult().observe(viewLifecycleOwner, Observer {
            loading.hide()
            it?.notifications?.let { notificationList: MutableList<NotificationModel> ->
                adapter.updateNotificationList(notificationList)
            }
        })
    }
}
