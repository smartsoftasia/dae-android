package pattararittigul.sasin.unesco_android.view.forgotpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeResetPassword
import pattararittigul.sasin.unesco_android.model.BaseResponse

class ForgotPasswordViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val forgotPasswordData = MutableLiveData<BaseResponse>()

    fun sendForgotPasswordRequest(email: String) {
        executeResetPassword(email)
            .subscribeBy(
                onNext = {
                    forgotPasswordData.postValue(it)
                },
                onError = {
                    forgotPasswordData.postValue(null)
                }
            )
            .addTo(compositeDisposable)
    }

    fun forgotPasswordResult(): LiveData<BaseResponse> = forgotPasswordData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
