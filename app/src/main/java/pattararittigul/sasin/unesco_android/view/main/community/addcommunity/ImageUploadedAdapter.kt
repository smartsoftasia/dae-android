package pattararittigul.sasin.unesco_android.view.main.community.addcommunity

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.TAG

class ImageUploadedAdapter(private val imageFiles: MutableList<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var callback: CommunityImageListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_image_added, parent, false)
        return ImageUploadedViewHolder(view)
    }

    override fun getItemCount(): Int {
        val size = imageFiles.size
        Log.d(TAG, "image size: $size")
        return size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ImageUploadedViewHolder) {
            val imageFile = imageFiles[position]
            holder.setupView(imageFile)
            holder.itemView.setOnClickListener {
                callback?.clickReselect(imageFile, position)
            }
        }
    }

    fun setListener(listener: CommunityImageListener) {
        callback = listener
    }

    fun updateImages(images: MutableList<String>) {
        Log.d(TAG, "image update: $images")
        imageFiles.clear()
        imageFiles.addAll(images)
        notifyDataSetChanged()
    }
}
