package pattararittigul.sasin.unesco_android.view.main.library

import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel

interface LibraryListener {
    fun onDownloadClick(book: LibraryAllResponseModel.Data.Category.Book)
}