package pattararittigul.sasin.unesco_android.view.main.menu.contactus

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.contact_us_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.finish
import pattararittigul.sasin.unesco_android.model.ContactUsResponseModel

class ContactUsFragment : Fragment() {

    private lateinit var mView: View

    companion object {
        private const val CALL_PHONE_PERMISSION_REQUEST_CODE = 490
        fun newInstance() = ContactUsFragment()
    }

    private lateinit var viewModel: ContactUsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.contact_us_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        mView.toolbar.setNavigationOnClickListener {
            finish()
        }
        mView.buttonPhone.setOnClickListener {
            prepareCallPhone()
        }
        mView.buttonEmail.setOnClickListener {
            composeEmail("Contact from DAE application")
        }
        mView.buttonWebsite.setOnClickListener {
            openUAEWebsite()
        }
    }

    private fun openUAEWebsite() {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri = Uri.parse(viewModel.contact.third)
        intent.data = uri
        startActivity(intent)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ContactUsViewModel::class.java)
        viewModel.getContactUs()
        observeContactUs()
    }

    private fun observeContactUs() {
        viewModel.contactUsResult().observe(viewLifecycleOwner, Observer {
            displayContactUsDetail(it)
        })
    }

    private fun displayContactUsDetail(contactUs: ContactUsResponseModel.Data?) {
        mView.buttonPhone.availableHandle(contactUs?.phone?.phone_display)
        mView.buttonEmail.availableHandle(contactUs?.email?.email_display)
        mView.buttonWebsite.availableHandle(contactUs?.website?.website_display)
    }

    private fun Button.availableHandle(contact: String?) {
        if (contact.isNullOrBlank()) {
            this.text = getString(R.string.unavailable)
            this.isEnabled = false
        } else {
            this.text = contact
            this.isEnabled = true
        }
    }


    private fun phoneCallIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(), android.Manifest.permission.CALL_PHONE
        ) != PackageManager.PERMISSION_GRANTED

    private fun requestPhoneCallPermission() {
        requestPermissions(
            arrayOf(android.Manifest.permission.CALL_PHONE),
            CALL_PHONE_PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CALL_PHONE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPhone()
                }
                return
            }
        }
    }

    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL).apply {
            data = Uri.parse("tel:${viewModel.contact.second}")
        }
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            startActivity(intent)
        }
    }


    private fun composeEmail(subject: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, Uri.parse(viewModel.contact.first))
            putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun prepareCallPhone() {
        if (phoneCallIsNotGrant()) {
            requestPhoneCallPermission()
        } else {
            callPhone()
        }
    }

}


