package pattararittigul.sasin.unesco_android.view.main.news.calendar

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel

class CalendarMonthAdapter(private val calendarMonths: MutableList<EventCalendarResponseModel.Data.CalendarMonth>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_new_month, parent, false)
        return CalendarMonthViewHolder(
            view
        )
    }

    override fun getItemCount(): Int = if (calendarMonths.isEmpty()) 0 else calendarMonths.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CalendarMonthViewHolder) {
            val month = calendarMonths[position]
            holder.setViewData(month)
        }
    }

    fun updateEvent(events: MutableList<EventCalendarResponseModel.Data.CalendarMonth>) {
        calendarMonths.clear()
        calendarMonths.addAll(events)
        notifyDataSetChanged()
    }
}