package pattararittigul.sasin.unesco_android.view.main.library


import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_sub_library.view.*
import pattararittigul.sasin.unesco_android.BuildConfig
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.enum.FileExtensionType
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.displayDialog
import pattararittigul.sasin.unesco_android.extension.displayErrorDialog
import pattararittigul.sasin.unesco_android.extension.loadingDialog
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel
import java.io.File

class LibraryCategoryFragment : Fragment() {

    private lateinit var viewModel: LibraryCategoryViewModel
    private val adapter = CategoryBookAdapter(mutableListOf())
    private lateinit var mView: View
    private val loading: ProgressDialog by lazy {
        requireContext().loadingDialog.apply {
            setCancelable(false)
        }
    }

    companion object {
        private const val ARGS_CATEGORY = "ARGS_CATEGORY"
        private const val EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 774

        fun newInstance(category: LibraryAllResponseModel.Data.Category): LibraryCategoryFragment {
            val fragment = LibraryCategoryFragment()
            val args = Bundle()
            args.putParcelable(ARGS_CATEGORY, category)
            fragment.arguments = args
            return fragment
        }
    }

    private fun openFolder(path: String) {
        val data = FileProvider.getUriForFile(
            requireContext(),
            "${BuildConfig.APPLICATION_ID}.provider",
            File(path)
        )

        requireContext().grantUriPermission(
            requireContext().packageName,
            data,
            Intent.FLAG_GRANT_READ_URI_PERMISSION
        )
        val type = data.requireDataType()
        Log.d(TAG, "$data should require type : $type")
        val intent = Intent(Intent.ACTION_VIEW)
            .setDataAndType(data, type)
            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        requireContext().startActivity(intent)
    }

    private fun Uri.requireDataType(): String {
        var typeMatch = "*/*"
        for ((extensions, type) in FileExtensionType.dataTypes) {
            for (it in extensions) {
                if ("${this}".contains(it, true)) {
                    typeMatch = type
                    break
                } else continue
            }
        }
        return typeMatch
    }


    private fun externalStorageIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED

    private fun requestExternalStoragePermission() {
        requestPermissions(
            arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LibraryCategoryViewModel::class.java)
        observerDownloadResult()
    }

    private fun observerDownloadResult() {
        viewModel.downloadResult().observe(viewLifecycleOwner, Observer {
            loading.dismiss()
            it?.let {
                displayDialog(
                    getString(R.string.downloaded_success),
                    getString(R.string.downloaded_book_desc_dialog),
                    getString(R.string.open),
                    DialogInterface.OnClickListener { dialog, _ ->
                        openFolder(it)
                        dialog.dismiss()
                    },
                    getString(R.string.later),
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    }
                )
            } ?: displayErrorDialog()
        })
    }

    private fun prepareToDownloadFile(book: LibraryAllResponseModel.Data.Category.Book) {
        if (externalStorageIsNotGrant()) {
            requestExternalStoragePermission()
        } else {
            displayDownLoadDialog(book)
        }
    }

    private fun readyToDownload(book: LibraryAllResponseModel.Data.Category.Book) {
        val name = when (book.title.toString().trim().length) {
            in 0 until 5 -> "untitle_${book.title}"
            in 5..19 -> book.title.toString()
            else -> book.title.toString().trim().substring(0, 15)
        }
        loading.show()
        viewModel.downloadFile("${book.download_url}", name)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_sub_library, container, false)
        val books: MutableList<LibraryAllResponseModel.Data.Category.Book>? =
            (arguments?.getParcelable(ARGS_CATEGORY) as? LibraryAllResponseModel.Data.Category)?.books
                ?: mutableListOf()
        initRecyclerView(books)
        return mView
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.holdingBook?.let { prepareToDownloadFile(it) }
                    Log.d(TAG, "grantResult: $grantResults")
                }
                return
            }
        }
    }

    private fun displayDownLoadDialog(book: LibraryAllResponseModel.Data.Category.Book) {
        displayDialog(
            "${book.title}",
            getString(R.string.download_book_desc_dialog),
            getString(R.string.download),
            DialogInterface.OnClickListener { dialog, _ ->
                readyToDownload(book)
                dialog.dismiss()
            },
            getString(R.string.cancel),
            DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
            , false)
    }

    private fun initRecyclerView(books: MutableList<LibraryAllResponseModel.Data.Category.Book>?) {
        mView.recyclerViewBook.adapter = adapter
        books?.let { adapter.updateBook(it) }

        adapter.setListener(object : LibraryListener {
            override fun onDownloadClick(book: LibraryAllResponseModel.Data.Category.Book) {
                book.download_url?.let {
                    viewModel.holdingBook = book
                    prepareToDownloadFile(book)
                } ?: displayErrorDialog()
            }
        })
    }
}
