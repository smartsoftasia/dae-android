package pattararittigul.sasin.unesco_android.view.main.community

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

class CommunityAdapter(private val newsList: MutableList<CommunityStoryResponseModel.Data.CommunicationStory>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var callback: CommunityCardListener? = null


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_community_card, parent, false)
        return CommunityCardViewHolder(view)
    }

    override fun getItemCount(): Int = newsList.size


    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val news = newsList[position]
        if (holder is CommunityCardViewHolder) {
            holder.itemView.setOnClickListener {
                callback?.clicked(news)
            }

            holder.setupView(news)
        }
    }

    fun setListener(listener: CommunityCardListener) {
        callback = listener
    }

    fun updateCommunityList(list: MutableList<CommunityStoryResponseModel.Data.CommunicationStory>) {
        newsList.clear()
        newsList.addAll(list)
        notifyDataSetChanged()
    }
}
