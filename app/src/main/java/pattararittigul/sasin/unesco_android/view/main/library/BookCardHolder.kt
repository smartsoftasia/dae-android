package pattararittigul.sasin.unesco_android.view.main.library

import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_library_card.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel

class BookCardHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun setupView(book: LibraryAllResponseModel.Data.Category.Book) {

        itemView.libraryTitle.text = book.title
        itemView.libraryDesc.text = book.author_name
//        itemView.textTime.text = book.date_created?.asTime()
//        itemView.textDate.text = book.date_created?.asDate()
        GlideApp.with(itemView.context)
            .load(Uri.parse(book.cover_image))
            .placeholder(R.drawable.place_holder_image_view)
            .error(R.drawable.place_holder_image_view)
            .centerCrop()
            .into(itemView.libraryBookCoverImage)

        itemView.buttonDownload.setOnClickListener {

        }
    }


}
