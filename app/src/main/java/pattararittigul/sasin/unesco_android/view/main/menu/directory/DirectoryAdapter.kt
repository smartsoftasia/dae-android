package pattararittigul.sasin.unesco_android.view.main.menu.directory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.AllDirectoryResponseModel

class DirectoryAdapter(private val directoryList: MutableList<AllDirectoryResponseModel.Data.ContactDirectory>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_directory, parent, false)
        return DirectoryViewHolder(view)
    }

    override fun getItemCount(): Int = directoryList.size

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (holder is DirectoryViewHolder) {
            val directory = directoryList[position]
            holder.setupView(directory)
        }
    }

    fun updateDirectories(directories: MutableList<AllDirectoryResponseModel.Data.ContactDirectory>) {
        directoryList.clear()
        directoryList.addAll(directories)
        notifyDataSetChanged()
    }
}