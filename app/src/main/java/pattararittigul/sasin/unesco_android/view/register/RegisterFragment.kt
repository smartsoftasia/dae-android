package pattararittigul.sasin.unesco_android.view.register

import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.register_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.*
import pattararittigul.sasin.unesco_android.view.login.LoginActivity


class RegisterFragment : Fragment(), TextWatcher {

    private lateinit var mView: View

    companion object {
        fun newInstance() = RegisterFragment()
    }

    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.register_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        mView.toolbar.setNavigationOnClickListener {
            finish()
        }

        mView.buttonRegister.setOnClickListener {
            when {
                !mView.inputEmail.text.toString().isEmailFormat() -> {
                    displayErrorDialog(message = R.string.email_pattern_error_desc)
                }
                passwordIsNotSame() -> displayPasswordNotMatchDialog()
                else -> viewModel.register(
                    mView.inputFirstName.text.toString(),
                    mView.inputLastName.text.toString(),
                    mView.inputHomeTown.text.toString(),
                    mView.inputEmail.text.toString(),
                    mView.inputPassword.text.toString()
                )
                //                toLoginActivity()
            }
        }
        mView.inputFirstName.addTextChangedListener(this)
        mView.inputLastName.addTextChangedListener(this)
        mView.inputEmail.addTextChangedListener(this)
        mView.inputPassword.addTextChangedListener(this)
        mView.inputConfirmPassword.addTextChangedListener(this)

    }

    private fun displayPasswordNotMatchDialog() {
        displayDialog(
            getString(R.string.dialog_sorry_title),
            getString(R.string.dialog_password_not_match_desc),
            getString(R.string.dialog_dismiss_button),
            DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() },
            false
        )
    }

    private fun toLoginActivity() {
        toActivity<LoginActivity>()
        requireActivity().finishAffinity()
    }

    override fun afterTextChanged(s: Editable?) {
        handleRegisterButton()
    }

    private fun handleRegisterButton() {
        mView.buttonRegister.isEnabled = allRequireFieldNotEmpty()
    }

    private fun allRequireFieldNotEmpty(): Boolean {
        return mView.inputFirstName.text.toString().isNotBlank() &&
                mView.inputLastName.text.toString().isNotBlank() &&
                mView.inputEmail.text.toString().isNotBlank() &&
                mView.inputPassword.text.toString().isNotBlank() &&
                mView.inputConfirmPassword.text.toString().isNotBlank()
    }

    private fun passwordIsNotSame() =
        mView.inputPassword.text.toString() != mView.inputConfirmPassword.text.toString()

    override fun beforeTextChanged(
        s: CharSequence?,
        start: Int,
        count: Int,
        after: Int
    ) {// To change body of created functions use File | Settings | File Templates.
    }

    override fun onTextChanged(
        s: CharSequence?,
        start: Int,
        before: Int,
        count: Int
    ) {// To change body of created functions use File | Settings | File Templates.
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        observer()
    }

    private fun observer() {
        viewModel.registerResult().observe(viewLifecycleOwner, Observer {

            it?.let { result ->

                if (result.status) {
                    toLoginActivity()
                } else {
                    displayErrorDialog(message = result.message)
                }
            } ?: displayErrorDialog()
        })
    }
}
