package pattararittigul.sasin.unesco_android.view.tutorial

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.tutorial_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.finish
import pattararittigul.sasin.unesco_android.extension.modalOutActivityAnimate

class TutorialFragment : Fragment() {
    private lateinit var mView: View
    private val adapter = TutorialAdapter(mutableListOf())

    companion object {
        fun newInstance() = TutorialFragment()
    }

    private lateinit var viewModel: TutorialViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.tutorial_fragment, container, false)
        init()
        return mView
    }

    override fun onResume() {
        super.onResume()
        SharedPreferences.setDisplayedTutorial(true)
    }

    private fun init() {
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        mView.recyclerViewTutorial.layoutManager = layoutManager
        mView.recyclerViewTutorial.setHasFixedSize(true)
        mView.recyclerViewTutorial.adapter = adapter
        mView.buttonHome.setOnClickListener {
            finish()
            modalOutActivityAnimate()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TutorialViewModel::class.java)
        adapter.updateList(viewModel.getTutorialList())
    }

}
