package pattararittigul.sasin.unesco_android.view.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.notification_badge.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.enum.MainNavigationMenu
import pattararittigul.sasin.unesco_android.extension.*
import pattararittigul.sasin.unesco_android.view.BaseActivity
import pattararittigul.sasin.unesco_android.view.main.community.MainCommunityFragment
import pattararittigul.sasin.unesco_android.view.main.library.MainLibraryFragment
import pattararittigul.sasin.unesco_android.view.main.menu.MainMenuFragment
import pattararittigul.sasin.unesco_android.view.main.news.MainNewsFragment
import pattararittigul.sasin.unesco_android.view.main.notification.MainNotificationFragment
import pattararittigul.sasin.unesco_android.view.tutorial.TutorialActivity

class MainActivity : BaseActivity() {

    private lateinit var viewModel: MainViewModel

    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                navView.selectedItemId -> {
                    return@OnNavigationItemSelectedListener false
                }
                R.id.navigation_community -> {
                    replaceFragmentWithTag(
                        R.id.container,
                        MainCommunityFragment.newInstance(),
                        "COMMUNITY"
                    )
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_library -> {
                    if (!navView.isSelected)
                        replaceFragmentWithTag(
                            R.id.container,
                            MainLibraryFragment.newInstance(),
                            "LIBRARY"
                        )
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_news -> {
                    replaceFragmentWithTag(R.id.container, MainNewsFragment.newInstance(), "NEWS")
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_notification -> {
                    clearBadge()
                    replaceFragmentWithTag(
                        R.id.container,
                        MainNotificationFragment.newInstance(),
                        "NOTIFICATION"
                    )
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_menu -> {
                    replaceFragmentWithTag(R.id.container, MainMenuFragment.newInstance(), "MENU")
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val directPage = intent?.extras?.getString("DIRECT_PAGE")
        setStatusBarColor(ContextCompat.getColor(this, R.color.color_primary_variant))
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        updateStatus()
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        if (directPage != null) {
            showScreen(directPage)
        } else {
            navView.menu.findItem(R.id.navigation_news).isChecked = true
            replaceFragmentNow(R.id.container, MainNewsFragment.newInstance())
        }
    }

    private fun updateStatus() {
        viewModel.validateInstanceId()
        viewModel.notificationResult().observe(this, Observer {
            it?.let { notification ->
                when {
                    notification > 10 -> {
                        displayBadge(MainNavigationMenu.NOTIFICATION, "9+")
                    }
                    notification > 0 -> {
                        displayBadge(MainNavigationMenu.NOTIFICATION, "$notification")
                    }
                    else -> {
                        clearBadge()
                    }
                }
            } ?: clearBadge()
        })
    }

    private fun showScreen(directPage: String) {
        when (directPage) {
            "COMMUNITY" -> {
                navView.menu.findItem(R.id.navigation_community).isChecked = true
                replaceFragmentNow(R.id.container, MainCommunityFragment.newInstance())
            }
            "MENU" -> {
                navView.menu.findItem(R.id.navigation_menu).isChecked = true
                replaceFragmentNow(R.id.container, MainMenuFragment.newInstance())
            }
        }
    }

    override fun onResume() {
        super.onResume()
        showTutorial()
    }

    private fun showTutorial() {
        if (!SharedPreferences.wasDisplayTutorial()) {
            displayTutorial()
        }
    }

    private fun displayTutorial() {
        toActivity<TutorialActivity>()
        modalInActivityAnimate()
    }

    private fun displayBadge(navigationMenu: MainNavigationMenu, text: String) {
        val navigationView: BottomNavigationMenuView =
            navView.getChildAt(0) as BottomNavigationMenuView
        val menuTab: BottomNavigationItemView =
            navigationView.getChildAt(navigationMenu.tabPosition) as BottomNavigationItemView
        val badge = LayoutInflater.from(this).inflate(
            R.layout.notification_badge,
            menuTab,
            true
        )
        if (text.isNotEmpty()) {
            badge.notificationsBadgeTextView.text = text
            badge.notificationsBadgeTextView.visibility = View.VISIBLE
        } else {
            badge.notificationsBadgeTextView.visibility = View.GONE
        }
    }

    private fun clearBadge(navigationMenu: MainNavigationMenu = MainNavigationMenu.NOTIFICATION) {
        displayBadge(navigationMenu, "")
    }
}
