package pattararittigul.sasin.unesco_android.view.main.menu.directory.adddirectory

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.request.target.ViewTarget
import kotlinx.android.synthetic.main.add_directory_fragment.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.GlideOptions
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class AddDirectoryFragment : Fragment(), TextWatcher {
    private lateinit var mView: View
    private val loading: ProgressDialog by lazy { requireContext().loadingDialog }
    private var selectedImage: File? = null

    companion object {
        private const val EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE = 525
        private const val REQUEST_CAMERA_CAPTURE = 424
        private const val EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 456
        private const val REQUEST_GALLERY_PHOTO = 223
        fun newInstance() =
            AddDirectoryFragment()
    }

    private lateinit var viewModel: AddDirectoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.add_directory_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        mView.toolbar.setNavigationOnClickListener {
            val returnIntent = Intent()
            requireActivity().setResult(Activity.RESULT_CANCELED, returnIntent)
            requireActivity().onBackPressed()
            hideKeyboard()
        }
        mView.editImageButton.setOnClickListener {
            val items = arrayOf(
                getString(R.string.camera),
                getString(R.string.gallery),
                getString(R.string.cancel)
            )
            displayItemDialog(items)
        }
        mView.buttonCreateDirectory.setOnClickListener {
            selectedImage?.let { image ->
                viewModel.addMember(
                    image,
                    mView.inputFullName.text.toString().dashIfEmpty(),
                    mView.inputPosition.text.toString().dashIfEmpty(),
                    mView.inputPhone.text.toString().dashIfEmpty(),
                    mView.inputEmail.text.toString().dashIfEmpty(),
                    mView.inputTownship.text.toString().dashIfEmpty()
                )
                loading.show()
            }
        }
    }

    private fun String?.dashIfEmpty(): String {
        return if (this.isNullOrBlank()) "–" else this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddDirectoryViewModel::class.java)
        observeAddMember()
    }

    private fun observeAddMember() {
        viewModel.addMemberResult().observe(viewLifecycleOwner, Observer {
            loading.dismiss()
            it?.let {
                when {
                    it.status -> {
                        finishWithSuccess()
                    }
                    else -> {
                        errorAlertDialog()
                    }
                }
            }
        })
    }


    private fun requestExternalStorageAndCameraPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
            EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE
        )
    }

    private fun finishWithSuccess() {
        val returnIntent = Intent()
        requireActivity().setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_GALLERY_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        try {
                            val fos: FileOutputStream?
                            val photoFile: File?
                            photoFile = createImageFile()
                            fos = FileOutputStream(photoFile)
                            getBitmapFromUri(it).compress(Bitmap.CompressFormat.JPEG, 50, fos)
                            fos.flush()
                            fos.close()
                            selectedImage = photoFile
                        } catch (e: Exception) {
                            Log.e("SAVE_IMAGE", e.message, e)
                        }
                    }.also {
                        Log.d(TAG, "image on activity result: $it")
                        selectedImage?.let { image ->
                            displayImageDirectory(image)
                        }
                    }
                }

            }
            REQUEST_CAMERA_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    selectedImage?.let { image -> displayImageDirectory(image) }

                }
            }
        }
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (photoFile != null) {
                val photoUri =
                    requireContext().getFileUri(photoFile)
                selectedImage = photoFile
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                this.startActivityForResult(
                    takePictureIntent,
                    REQUEST_CAMERA_CAPTURE
                )
            }
        }
    }

    private fun prepareToDispatchTakePicture() {
        if (cameraAndExternalStorageIsNotGrant()) {
            requestExternalStorageAndCameraPermission()
        } else {
            dispatchTakePictureIntent()
        }
    }

    private fun cameraAndExternalStorageIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED


    private fun displayItemDialog(buttons: Array<String>) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setItems(buttons) { dialog, which ->
            when (buttons[which]) {
                buttons[0] -> {
                    prepareToDispatchTakePicture()
                    dialog.dismiss()
                }
                buttons[1] -> {
                    prepareToDispatchGallery()
                    dialog.dismiss()
                }
                buttons[2] -> {
                    dialog.dismiss()
                }
            }
        }.also { it.show() }
    }


    override fun afterTextChanged(s: Editable?) {
        updateButtonCreate()
    }


    private fun updateButtonCreate() {
        mView.buttonCreateDirectory.isEnabled =
            mView.inputFullName.text.toString().isNotBlank()
                    && mView.inputPosition.text.toString().isNotBlank()
                    && mView.inputEmail.text.toString().isNotBlank()
                    && selectedImage != null
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

    private fun displayImageDirectory(image: File): ViewTarget<ImageView, Drawable> {
        return GlideApp.with(requireContext())
            .load(image)
            .circleCrop()
            .placeholder(R.drawable.place_holder_profile)
            .error(R.drawable.place_holder_profile)
            .apply(GlideOptions.circleCropTransform())
            .into(mView.imageDirectory)
    }

    private fun prepareToDispatchGallery() {
        if (externalStorageIsNotGrant()) {
            requestExternalStoragePermission()
        } else {
            dispatchGalleryIntent()
        }
    }

    private fun dispatchGalleryIntent() {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO)
    }

    private fun externalStorageIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED

    private fun requestExternalStoragePermission() {
        requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchGalleryIntent()
                }
                return
            }
            EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent()
                }
                return
            }
        }
    }

}
