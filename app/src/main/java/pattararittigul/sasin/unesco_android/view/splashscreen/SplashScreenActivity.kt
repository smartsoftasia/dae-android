package pattararittigul.sasin.unesco_android.view.splashscreen

import android.os.Bundle
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.view.BaseActivity
import java.util.*

class SplashScreenActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setDefaultLanguage(Locale("en"))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SplashScreenFragment.newInstance())
                .commitNow()
        }
    }

}
