package pattararittigul.sasin.unesco_android.view.main.community

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

class RecommendPagerAdapter(
    fragmentManager: FragmentManager,
    val recommends: MutableList<CommunityStoryResponseModel.Data.CommunicationStory>
) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment =
        RecommendCardFragment.newInstance(recommends[position])

    override fun getCount(): Int = recommends.count()
}