package pattararittigul.sasin.unesco_android.view.tutorial

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_tutorial_left.view.*
import kotlinx.android.synthetic.main.item_tutorial_right.view.*
import pattararittigul.sasin.unesco_android.enum.TutorialItemStyle
import pattararittigul.sasin.unesco_android.model.TutorialItemModel

class TutorialViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    fun setupView(item: TutorialItemModel) {
        if (item.style.type == TutorialItemStyle.LEFT.type) {
            itemView.imageL.setImageDrawable(ContextCompat.getDrawable(view.context, item.image))
            itemView.headerL.setText(item.header)
            itemView.descriptionL.setText(item.description)
        } else {
            itemView.imageR.setImageDrawable(ContextCompat.getDrawable(view.context, item.image))
            itemView.headerR.setText(item.header)
            itemView.descriptionR.setText(item.description)
        }
    }


}
