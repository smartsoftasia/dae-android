package pattararittigul.sasin.unesco_android.view.main.menu.directory

import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.directory_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.finish
import pattararittigul.sasin.unesco_android.extension.hideKeyboard
import pattararittigul.sasin.unesco_android.extension.loadingDialog
import pattararittigul.sasin.unesco_android.extension.toActivityForResult
import pattararittigul.sasin.unesco_android.view.main.menu.directory.adddirectory.AddDirectoryActivity

class DirectoryFragment : Fragment() {

    private lateinit var mView: View
    private val adapter = DirectoryAdapter(mutableListOf())
    private val loading: ProgressDialog by lazy {
        requireContext().loadingDialog.apply {
            setCancelable(false)
        }
    }

    companion object {
        private const val ADD_DIRECTORY_REQUEST_CODE = 546
        fun newInstance() =
            DirectoryFragment()
    }

    private lateinit var viewModel: DirectoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.directory_fragment, container, false)
        init()
        setRecyclerView()
        return mView
    }

    private fun setRecyclerView() {
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        mView.recyclerViewDirectory.layoutManager = layoutManager
        mView.recyclerViewDirectory.setHasFixedSize(true)
        mView.recyclerViewDirectory.adapter = adapter
    }

    private fun init() {
        mView.toolbar.setNavigationOnClickListener {
            finish()
        }
        mView.addButtonFab.setOnClickListener {
            toActivityForResult<AddDirectoryActivity>(ADD_DIRECTORY_REQUEST_CODE)
        }
        mView.inputSearch.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    performSearch(mView.inputSearch.text.toString().trim())
                    true
                }
                else -> false
            }
        }

        mView.inputSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    viewModel.fetchDirectory()
                    loading.show()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })
    }

    private fun performSearch(input: String) {
        if (input.isNotBlank()) {
            viewModel.fetchDirectory(input)
        } else {
            viewModel.fetchDirectory()
        }
        hideKeyboard()
        loading.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ADD_DIRECTORY_REQUEST_CODE -> {
                if (resultCode == RESULT_OK) {
                    viewModel.fetchDirectory()
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DirectoryViewModel::class.java)
        viewModel.fetchDirectory()
        loading.show()
        observeDirectory()
    }

    private fun observeDirectory() {
        viewModel.directoryResult().observe(viewLifecycleOwner, Observer {
            loading.dismiss()
            it?.let {
                it.contact_directories?.let { list -> adapter.updateDirectories(list) }
            }
        })
    }

}
