package pattararittigul.sasin.unesco_android.view.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.executeGetDetail
import pattararittigul.sasin.unesco_android.extension.executeUpdateProfile

class MainViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val notificationData = MutableLiveData<Int>()

    private fun getUserDetail(id: String = "${SharedPreferences.getUserDetail().id}") {
        executeGetDetail(id)
            .subscribeBy(
                onNext = {
                    notificationData.postValue(it.data?.count_notification)
                },
                onError = {
                    notificationData.postValue(0)
                }
            ).addTo(compositeDisposable)
    }


    fun validateInstanceId() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.e(TAG, "getInstanceId failed", task.exception)
                    getUserDetail()
                    return@OnCompleteListener
                }
                val token = task.result?.token
                updateTokenToServer(token)
                Log.d(TAG, "current instanceId: $token")
            })
    }

    fun notificationResult(): LiveData<Int> = notificationData

    private fun updateTokenToServer(
        token: String?,
        id: String = "${SharedPreferences.getUserDetail().id}"
    ) {
        Log.d(TAG, "Token ")
        executeUpdateProfile(
            id,
            appToken = token,
            firstName = null,
            profile = null,
            hometown = null,
            lastName = null
        ).subscribeBy(
            onNext = { getUserDetail() },
            onError = { getUserDetail() }
        ).addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}