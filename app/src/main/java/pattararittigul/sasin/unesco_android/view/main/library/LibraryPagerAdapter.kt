package pattararittigul.sasin.unesco_android.view.main.library

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel

class LibraryPagerAdapter(
    fragmentManager: FragmentManager,
    private val categoryList: MutableList<LibraryAllResponseModel.Data.Category>
) :
    FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return LibraryCategoryFragment.newInstance(categoryList[position])
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return categoryList[position].category_name
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getCount(): Int = categoryList.size

    fun updateTabs(categories: MutableList<LibraryAllResponseModel.Data.Category>) {
        categoryList.clear()
        categoryList.addAll(categories)
        notifyDataSetChanged()
    }
}
