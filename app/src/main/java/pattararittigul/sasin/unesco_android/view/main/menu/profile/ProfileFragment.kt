package pattararittigul.sasin.unesco_android.view.main.menu.profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.request.target.ViewTarget
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.about_us_fragment.view.toolbar
import kotlinx.android.synthetic.main.profile_fragment.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.GlideOptions
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ProfileFragment : Fragment() {
    private lateinit var mView: View
    private var isEditMode = false
    private val loading: ProgressDialog by lazy { requireContext().loadingDialog }
    private val callbackManager by lazy { CallbackManager.Factory.create() }
    private var selectedImage: File? = null


    companion object {

        private const val EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE = 555
        private const val REQUEST_CAMERA_CAPTURE = 434
        private const val EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 556
        private const val REQUEST_GALLERY_PHOTO = 775
        fun newInstance() =
            ProfileFragment()
    }

    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.profile_fragment, container, false)
        FacebookSdk.fullyInitialize()
        LoginManager.getInstance()
            ?.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    loading.dismiss()
                    val profile: Profile? = Profile.getCurrentProfile()
                    profile?.let {
                        viewModel.updateProfile(
                            firstName = it.firstName,
                            lastName = it.lastName
                        )

                        GlideApp.with(requireContext()).load(profile.getProfilePictureUri(100, 100))
                            .circleCrop()
                            .placeholder(R.drawable.place_holder_profile)
                            .error(R.drawable.place_holder_profile)
                            .into(mView.imageProfile)
                    }
                }

                override fun onCancel() {
                    loading.dismiss()
                }

                override fun onError(error: FacebookException?) {
                    loading.dismiss()
                }
            })
        init()
        return mView
    }

    private fun requestExternalStorageAndCameraPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
            EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_GALLERY_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        try {
                            val fos: FileOutputStream?
                            val photoFile: File?
                            photoFile = createImageFile()
                            fos = FileOutputStream(photoFile)
                            getBitmapFromUri(it).compress(Bitmap.CompressFormat.JPEG, 100, fos)
                            fos.flush()
                            fos.close()
                            selectedImage = photoFile
                        } catch (e: Exception) {
                            Log.e("SAVE_IMAGE", e.message, e)
                        }
                        Log.d(TAG, "image on activity result: $it")
                        selectedImage?.let { image ->
                            displayProfile(image)
                        }
                    }
                }
            }
            REQUEST_CAMERA_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    selectedImage?.let { image -> displayProfile(image) }

                }
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun displayProfile(image: File): ViewTarget<ImageView, Drawable> {
        return GlideApp.with(requireContext())
            .load(image)
            .placeholder(R.drawable.place_holder_profile)
            .error(R.drawable.place_holder_profile)
            .apply(GlideOptions.circleCropTransform())
            .into(mView.imageProfile)
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        mView.toolbar.setNavigationOnClickListener {
            finish()
        }
        mView.toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_edit -> {
                    changeToEditMode()
                    return@setOnMenuItemClickListener true
                }
                R.id.menu_cancel -> {
                    changeToDisplayMode()
                    return@setOnMenuItemClickListener true
                }
                else -> false
            }
        }

        mView.editImageButton.setOnClickListener {
            val items = arrayOf(
                getString(R.string.camera),
                getString(R.string.gallery),
                getString(R.string.cancel)
            )

            displayItemDialog(items)
        }

        mView.buttonUpdateProfile.setOnClickListener {
            if (mView.inputFirstName.text.toString().isNotBlank() && mView.inputLastName.text.toString().isNotBlank()) {
                loading.show()
                viewModel.updateProfile(
                    mView.inputFirstName.text.toString(),
                    mView.inputLastName.text.toString(),
                    mView.inputHomeTown.text.toString(),
                    selectedImage
                )
            }
        }
        mView.buttonLoginWithFacebook.setOnClickListener {
            updateFacebookProfile()
        }
    }

    private fun displayItemDialog(buttons: Array<String>) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setItems(buttons) { dialog, which ->
            when (buttons[which]) {
                buttons[0] -> {
                    prepareToDispatchTakePicture()
                    dialog.dismiss()
                }
                buttons[1] -> {
                    prepareToDispatchGallery()
                    dialog.dismiss()
                }
                buttons[2] -> {
                    dialog.dismiss()
                }
            }
        }.also { it.show() }
    }


    private fun prepareToDispatchGallery() {
        if (externalStorageIsNotGrant()) {
            requestExternalStoragePermission()
        } else {
            dispatchGalleryIntent()
        }
    }

    private fun dispatchGalleryIntent() {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO)
    }

    private fun externalStorageIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED

    private fun requestExternalStoragePermission() {
        requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE
        )
    }

    private fun updateFacebookProfile() {
        loading.show()
        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedInWithFacebook =
            accessToken != null && !accessToken.isExpired
        if (isLoggedInWithFacebook) {
            LoginManager.getInstance().logInWithReadPermissions(this, arrayListOf("public_profile"))
        }
    }

    private fun changeToDisplayMode() {
        isEditMode = false
        handleEditVisibilityMode()
        hideKeyboard()
    }

    private fun changeToEditMode() {
        isEditMode = true
        handleEditVisibilityMode()
        handleInputEditText()
    }

    private fun handleInputEditText() {
        SharedPreferences.getUserDetail().apply {
            mView.inputFirstName.withText(first_name)
            mView.inputLastName.withText(last_name)
            mView.inputHomeTown.withText(howetown)
        }
    }

    private fun EditText.withText(text: String?) {
        if (!text.isNullOrBlank()) {
            this.let {
                it.setText(text, TextView.BufferType.EDITABLE)
                it
            }.also {
                it.setCursorAtEnd()
            }
        }
    }


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (photoFile != null) {
                val photoUri =
                    requireContext().getFileUri(photoFile)
                selectedImage = photoFile
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                this.startActivityForResult(
                    takePictureIntent,
                    REQUEST_CAMERA_CAPTURE
                )
            }
        }
    }

    private fun prepareToDispatchTakePicture() {
        if (cameraAndExternalStorageIsNotGrant()) {
            requestExternalStorageAndCameraPermission()
        } else {
            dispatchTakePictureIntent()
        }
    }

    private fun cameraAndExternalStorageIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchGalleryIntent()
                }
                return
            }
            EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent()
                }
                return
            }
        }
    }

    private fun EditText.setCursorAtEnd() {
        this.setSelection(this.text.toString().length)
    }

    private fun handleEditVisibilityMode() {
        mView.toolbar.menu.findItem(R.id.menu_edit).isVisible = !isEditMode
        mView.toolbar.menu.findItem(R.id.menu_cancel).isVisible = isEditMode
        mView.editImageButton.isVisible = isEditMode
        mView.inputFirstName.isVisible = isEditMode
        mView.inputLastName.isVisible = isEditMode
        mView.inputHomeTown.isVisible = isEditMode
        mView.buttonUpdateProfile.isVisible = isEditMode
        mView.buttonLoginWithFacebook.isVisible = !isEditMode
        mView.textFullName.isVisible = !isEditMode
        mView.textHometown.isVisible = !isEditMode
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        viewModel.updateDisplayData()
        viewModel.executeGetProfile()
        observe()
    }

    @SuppressLint("SetTextI18n")
    private fun observe() {
        viewModel.profileResult().observe(viewLifecycleOwner, Observer {
            it?.let {
                loading.dismiss()
                changeToDisplayMode()
                Log.d(TAG, "observe: $it")
                mView.textFullName.text = "${it.first_name} ${it.last_name}"
                mView.textHometown.text = it.howetown
                it.profile?.let { image ->
                    GlideApp.with(requireContext()).load(Uri.parse(image))
                        .placeholder(R.drawable.place_holder_profile)
                        .error(R.drawable.place_holder_profile)
                        .apply(GlideOptions.circleCropTransform())
                        .into(mView.imageProfile)
                }
            }
        })
    }

}
