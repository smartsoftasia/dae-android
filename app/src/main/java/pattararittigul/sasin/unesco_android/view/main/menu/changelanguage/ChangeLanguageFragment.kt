package pattararittigul.sasin.unesco_android.view.main.menu.changelanguage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.change_language_fragment.view.*
import pattararittigul.sasin.unesco_android.util.LocaleManager
import pattararittigul.sasin.unesco_android.view.BaseActivity
import java.util.*


class ChangeLanguageFragment : Fragment() {
    private lateinit var mView: View

    companion object {
        fun newInstance() =
            ChangeLanguageFragment()
    }

    private lateinit var viewModel: ChangeLanguageViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(
            pattararittigul.sasin.unesco_android.R.layout.change_language_fragment,
            container,
            false
        )
        init()
        return mView
    }

    private fun init() {
        handleLanguageButton()

        mView.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        mView.buttonEnglishLanguage.setOnClickListener {
            (requireActivity() as BaseActivity).setLanguage(LocaleManager.ENGLISH)
            LocaleManager.updateToLocale(requireContext().applicationContext, LocaleManager.ENGLISH)
        }
        mView.buttonMyanmarLanguage.setOnClickListener {
            (requireActivity() as BaseActivity).setLanguage(LocaleManager.MYANMAR)
            LocaleManager.updateToLocale(requireContext().applicationContext, LocaleManager.MYANMAR)
        }
    }

    private fun handleLanguageButton() {

        val currentLanguage: Locale = (requireActivity() as BaseActivity).getCurrentLanguage()
        mView.buttonMyanmarLanguage.isEnabled =
            currentLanguage.toLanguageTag() == LocaleManager.ENGLISH
        mView.buttonEnglishLanguage.isEnabled =
            currentLanguage.toLanguageTag() == LocaleManager.MYANMAR


        mView.toolbar.setTitle(pattararittigul.sasin.unesco_android.R.string.change_language)
        mView.toolbar.title =
            getString(pattararittigul.sasin.unesco_android.R.string.change_language)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChangeLanguageViewModel::class.java)
    }

}
