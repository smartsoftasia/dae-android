package pattararittigul.sasin.unesco_android.view.main.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.main_news_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.hideKeyboard

class MainNewsFragment : Fragment() {
    private lateinit var mView: View
    private val newsPagerAdapter by lazy { NewsPagerAdapter(childFragmentManager, mutableListOf()) }
    private var currentTab = 0

    companion object {
        fun newInstance() = MainNewsFragment()
    }

    private lateinit var viewModel: MainNewsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.main_news_fragment, container, false)
        setupTabLayout()
        setupViewPager()
        return mView
    }

    private fun setupViewPager() {
        mView.viewPager.adapter = newsPagerAdapter
    }

    private fun setupTabLayout() {
        mView.tabLayout.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) = Unit

            override fun onTabUnselected(tab: TabLayout.Tab?) = Unit

            override fun onTabSelected(tab: TabLayout.Tab) {
                currentTab = mView.viewPager.currentItem
                mView.viewPager.setCurrentItem(currentTab, true)
                hideKeyboard()
            }
        })
        createTabs()
    }

    private fun createTabs() {
        val tabNames = mutableListOf(getString(R.string.news), getString(R.string.calendar))
        mView.tabLayout.removeAllTabs()
        newsPagerAdapter.updateTabs(tabNames)
        mView.tabLayout.addTab(createTab(tabNames[0]), true)
        mView.tabLayout.addTab(createTab(tabNames[1]), false)
        mView.tabLayout.setupWithViewPager(mView.viewPager)
        newsPagerAdapter.notifyDataSetChanged()
    }

    private fun createTab(name: String): TabLayout.Tab {
        return mView.tabLayout.newTab()
            .also {
                it.text = name
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainNewsViewModel::class.java)
    }

}
