package pattararittigul.sasin.unesco_android.view.main.community

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.recommend_card_fragment.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.toActivityWith
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel
import pattararittigul.sasin.unesco_android.view.main.community.communitydetail.CommunityDetailActivity

class RecommendCardFragment : Fragment() {
    private lateinit var mView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.recommend_card_fragment, container, false)
        val story: CommunityStoryResponseModel.Data.CommunicationStory? =
            arguments?.getParcelable("STORY_RECOMMEND")
        story?.let {
            init(it)
        }
        return mView
    }

    private fun init(story: CommunityStoryResponseModel.Data.CommunicationStory) {
        if (story.images?.isNotEmpty() == true) {
            story.images?.get(0)?.let {
                GlideApp
                    .with(requireContext())
                    .load(Uri.parse(it))
                    .placeholder(R.drawable.place_holder_image_view)
                    .error(R.drawable.place_holder_image_view)
                    .into(mView.imageRecommendCard)
            }
        }
        mView.buttonMoreDetail.setOnClickListener {
            toActivityWith<CommunityDetailActivity>("COMMUNITY_STORY", story)
        }
        mView.textTitle.text = story.title
        mView.textDescription.text = story.description_content
        mView.textCountComment.text =
            "${story?.comments?.size} ${requireContext().getString(R.string.suffix_comments)}"
    }

    companion object {
        fun newInstance(communityStory: CommunityStoryResponseModel.Data.CommunicationStory): RecommendCardFragment {
            val fragment = RecommendCardFragment()
            val args = Bundle()
            args.putParcelable("STORY_RECOMMEND", communityStory)
            fragment.arguments = args
            return fragment
        }
    }

}