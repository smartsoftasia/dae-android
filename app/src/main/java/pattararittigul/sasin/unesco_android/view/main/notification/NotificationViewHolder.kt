package pattararittigul.sasin.unesco_android.view.main.notification

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_notification.view.*
import pattararittigul.sasin.unesco_android.model.NotificationModel

class NotificationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setupView(notification: NotificationModel) {
        itemView.notificationTitle.text = notification.title
        itemView.notificationDesc.text = notification.short_description
    }
}
