package pattararittigul.sasin.unesco_android.view.main.menu.aboutus

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.about_us_fragment.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.finish
import pattararittigul.sasin.unesco_android.model.AboutUsResponseModel


class AboutUsFragment : Fragment() {

    companion object {
        fun newInstance() =
            AboutUsFragment()
    }

    private lateinit var viewModel: AboutUsViewModel
    private lateinit var mView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.about_us_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        mView.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AboutUsViewModel::class.java)
        viewModel.getAboutUs()
        observeAboutUsResult()
    }

    private fun observeAboutUsResult() {
        viewModel.aboutUsResult().observe(viewLifecycleOwner, Observer {
            it?.let {
                displayDetail(it)
            }
        })
    }

    private fun displayDetail(detail: AboutUsResponseModel.Data) {
        GlideApp.with(requireContext()).load(Uri.parse(detail.about_us_content))
            .placeholder(R.drawable.place_holder_image_view)
            .error(R.drawable.place_holder_image_view)
            .centerCrop()
            .into(mView.imageAboutUs)

        mView.textAboutUs.text = detail.about_us_content
    }

}
