package pattararittigul.sasin.unesco_android.view.main.community.communitydetail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.setStatusBarColor
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel
import pattararittigul.sasin.unesco_android.view.BaseActivity

class CommunityDetailActivity : BaseActivity() {

    private val fragment = CommunityDetailFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(ContextCompat.getColor(this, R.color.color_primary_variant))
        setContentView(R.layout.community_detail_activity)

        if (savedInstanceState == null) {
            (intent?.extras?.getParcelable("COMMUNITY_STORY") as? CommunityStoryResponseModel.Data.CommunicationStory)?.let {
                supportFragmentManager.beginTransaction()
                    .replace(
                        R.id.container,
                        fragment.newInstance(it)
                    )
                    .commitNow()
            }
        }
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

}
