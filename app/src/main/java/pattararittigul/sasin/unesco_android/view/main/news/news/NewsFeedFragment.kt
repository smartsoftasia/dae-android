package pattararittigul.sasin.unesco_android.view.main.news.news

import android.app.ProgressDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.news_feed_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.displayErrorDialog
import pattararittigul.sasin.unesco_android.extension.hideKeyboard
import pattararittigul.sasin.unesco_android.extension.loadingDialog
import pattararittigul.sasin.unesco_android.extension.toActivityWith
import pattararittigul.sasin.unesco_android.model.NewsResponseModel
import pattararittigul.sasin.unesco_android.view.main.news.news.newsdetail.NewsDetailActivity

class NewsFeedFragment : Fragment() {

    private lateinit var mView: View
    private val adapter = NewsFeedAdapter(mutableListOf())
    private val loading: ProgressDialog by lazy {
        requireContext().loadingDialog.apply {
            setCancelable(false)
        }
    }

    companion object {
        fun newInstance() = NewsFeedFragment()
    }

    private lateinit var viewModel: NewsFeedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.news_feed_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        adapter.setListener(object : NewsEventListener {
            override fun newsClick(news: NewsResponseModel.Data.NewsStory) {
                toActivityWith<NewsDetailActivity>("NEWS_DETAIL", news)
            }
        })
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        mView.recyclerViewNews.layoutManager = layoutManager
        mView.recyclerViewNews.setHasFixedSize(true)
        mView.recyclerViewNews.adapter = adapter

        mView.swipeRefresh.setOnRefreshListener {
            viewModel.fetchNews()
        }

        mView.inputSearch.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    performSearch(mView.inputSearch.text.toString().trim())
                    true
                }
                else -> false
            }
        }

        mView.inputSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    viewModel.fetchNews()
                    loading.show()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {// To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {// To change body of created functions use File | Settings | File Templates.
            }
        })
    }

    private fun performSearch(input: String) {
        if (input.isNotBlank()) {
            viewModel.fetchNews(input)
        } else {
            viewModel.fetchNews()
        }
        hideKeyboard()
        loading.show()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(NewsFeedViewModel::class.java)
        viewModel.fetchNews()
        loading.show()
        observeNewsResult()
    }

    private fun observeNewsResult() {
        viewModel.newsResult().observe(viewLifecycleOwner, Observer {
            loading.dismiss()
            mView.swipeRefresh.isRefreshing = false
            it?.news_stories?.let { news -> adapter.updateNews(news) } ?: displayErrorDialog()
        })
    }

}
