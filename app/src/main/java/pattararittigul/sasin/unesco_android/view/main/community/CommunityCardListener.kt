package pattararittigul.sasin.unesco_android.view.main.community

import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

interface CommunityCardListener {
    fun clicked(story: CommunityStoryResponseModel.Data.CommunicationStory)
}