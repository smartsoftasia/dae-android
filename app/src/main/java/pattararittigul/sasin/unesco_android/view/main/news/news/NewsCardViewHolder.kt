package pattararittigul.sasin.unesco_android.view.main.news.news

import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_news_card.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.asDate
import pattararittigul.sasin.unesco_android.model.NewsResponseModel

class NewsCardViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setupView(news: NewsResponseModel.Data.NewsStory) {
        GlideApp.with(itemView).load(Uri.parse(news.images?.get(0)))
            .centerCrop()
            .placeholder(R.drawable.place_holder_image_view)
            .error(R.drawable.place_holder_image_view)
            .into(itemView.imageNews)
        itemView.imageNews
        itemView.textCountView.text =
            "${news.total_viewed_amount} ${itemView.context.getString(R.string.suffix_views)}"
        itemView.textDate.text = news.date_created?.asDate()
        itemView.textTitle.text = news.title
        itemView.textDescription.text = news.description_content
    }

}
