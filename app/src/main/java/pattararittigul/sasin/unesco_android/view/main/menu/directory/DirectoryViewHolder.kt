package pattararittigul.sasin.unesco_android.view.main.menu.directory

import android.annotation.SuppressLint
import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_directory.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.AllDirectoryResponseModel

class DirectoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    @SuppressLint("SetTextI18n")
    fun setupView(directory: AllDirectoryResponseModel.Data.ContactDirectory) {

        itemView.textDirectoryName.text =
            "${directory.profile_fullname}"
        itemView.textDirectoryEmail.text =
            "(${directory.profile_email})"
        itemView.textDirectoryPhone.text =
            "${directory.profile_phone}"

        itemView.textDirectoryPosition.text = directory.profile_organization

        itemView.textDirectoryTownship.text = directory.profile_township

        GlideApp.with(itemView).load(Uri.parse(directory.profile_image))
            .circleCrop()
            .placeholder(R.drawable.place_holder_profile_small)
            .error(R.drawable.place_holder_profile_small)
            .into(itemView.imageDirectory)
    }

}
