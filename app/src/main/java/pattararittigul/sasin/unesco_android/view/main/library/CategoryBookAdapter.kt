package pattararittigul.sasin.unesco_android.view.main.library

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_library_card.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel

class CategoryBookAdapter(private val bookList: MutableList<LibraryAllResponseModel.Data.Category.Book>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var callback: LibraryListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_library_card, parent, false)
        return BookCardHolder(view)

    }

    override fun getItemCount(): Int = bookList.size

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (holder is BookCardHolder) {
            val book = bookList[position]
            holder.setupView(book)
            holder.itemView.buttonDownload.setOnClickListener {
                callback?.onDownloadClick(book)
            }
        }
    }

    fun setListener(listener: LibraryListener) {
        callback = listener
    }

    fun updateBook(books: MutableList<LibraryAllResponseModel.Data.Category.Book>) {
        bookList.clear()
        bookList.addAll(books)
        notifyDataSetChanged()
    }
}
