package pattararittigul.sasin.unesco_android.view.main.menu.directory.adddirectory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.executeAddDirectory
import pattararittigul.sasin.unesco_android.model.BaseResponse
import java.io.File

class AddDirectoryViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val addDirectoryData = MutableLiveData<BaseResponse>()


    fun addMember(
        image: File,
        name: String?,
        position: String?,
        phone: String?,
        email: String?,
        township: String?
    ) {

        executeAddDirectory(
            "${SharedPreferences.getUserDetail().id}",
            image, name, position, phone, email, township
        ).subscribeBy(
            onNext = { addDirectoryData.postValue(it) },
            onError = { addDirectoryData.postValue(null) }
        ).addTo(compositeDisposable)
    }

    fun addMemberResult(): LiveData<BaseResponse> = addDirectoryData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
