package pattararittigul.sasin.unesco_android.view.main.news.calendar

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.calendar_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.loadingDialog

class CalendarFragment : Fragment() {

    private lateinit var mView: View
    private val calendarMonthAdapter = CalendarMonthAdapter(mutableListOf())
    private val loading: ProgressDialog by lazy { requireContext().loadingDialog }

    companion object {
        fun newInstance() = CalendarFragment()
    }

    private lateinit var viewModel: CalendarViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.calendar_fragment, container, false)
        setupCalendarRecyclerView()
        return mView
    }

    private fun setupCalendarRecyclerView() {
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        mView.recyclerViewMonth.layoutManager = layoutManager
        mView.recyclerViewMonth.setHasFixedSize(true)
        mView.recyclerViewMonth.adapter = calendarMonthAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CalendarViewModel::class.java)
        viewModel.getAllEvent()
        loading.show()
        observeEvent()
    }

    private fun observeEvent() {
        viewModel.eventCalendarResult().observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "calendar: $it")
            loading.dismiss()
            it?.calendar_months?.let { months ->
                calendarMonthAdapter.updateEvent(months)
            }
        })
    }
}
