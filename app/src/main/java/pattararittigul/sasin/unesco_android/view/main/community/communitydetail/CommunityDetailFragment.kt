package pattararittigul.sasin.unesco_android.view.main.community.communitydetail

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.community_detail_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.*
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel


class CommunityDetailFragment : Fragment(), TextWatcher {

    override fun afterTextChanged(s: Editable?) {
        updateButtonPushComment()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit


    private var story: CommunityStoryResponseModel.Data.CommunicationStory? = null
    private lateinit var mView: View
    private lateinit var viewModel: CommunityDetailViewModel
    private val adapter = CommentAdapter(mutableListOf())

    companion object {
        fun newInstance(story: CommunityStoryResponseModel.Data.CommunicationStory): CommunityDetailFragment {
            Log.d(TAG, "story==============>: ${story.is_liked}")
            val fragment = CommunityDetailFragment()
            val args = Bundle()
            args.putParcelable("STORY", story)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(
            R.layout.community_detail_fragment,
            container,
            false
        )
        story = arguments?.getParcelable("STORY")
        init(story)
        return mView
    }

    @SuppressLint("SetTextI18n")
    private fun init(mStory: CommunityStoryResponseModel.Data.CommunicationStory?) {
        mStory?.images?.let { initViewPager(it) }
        Log.d(TAG, "story: $mStory")
        mView.toolbar.setNavigationOnClickListener {
            returnIntentSuccess()
        }
        mView.toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
                    shareToDefault(story?.title, story?.description_content)
                    true
                }
                else -> false
            }
        }
        mView.shareFacebookButton.setOnClickListener {
            shareToFacebook(Uri.parse(story?.images?.get(0)), story?.description_content)
        }

        mView.shareLineButton.setOnClickListener {
            shareToLine(story?.description_content)
        }

        mView.shareTwitterButton.setOnClickListener {
            shareToTwitter(story?.description_content)
        }

        mView.inputComment.addTextChangedListener(this)

        mStory?.let {
            displayContent(it)
            displayLike(it)
        }

        initRecyclerViewComment(mStory?.comments)

        mView.buttonPushComment.setOnClickListener {
            viewModel.submitComment(mView.inputComment.text.toString())
            mView.inputComment.text.clear()
            hideKeyboard()
        }
    }

    private fun returnIntentSuccess() {
        val returnIntent = Intent()
        requireActivity().setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    private fun hasCommentContent(): Boolean = mView.inputComment.text.toString().isNotBlank()

    private fun updateButtonPushComment() {
        mView.buttonPushComment.isEnabled = hasCommentContent()
    }

    @SuppressLint("SetTextI18n")
    private fun displayLike(mStory: CommunityStoryResponseModel.Data.CommunicationStory?) {
        mStory?.is_liked?.let {
            updateLikeStatus(it)
        }
        mView.textCountLike.text =
            "${mStory?.count_like} ${getString(pattararittigul.sasin.unesco_android.R.string.suffix_like)}"
    }

    private fun updateLikeStatus(isLike: Boolean) {
        mView.likeCheckBox.isChecked = isLike
    }

    private fun initRecyclerViewComment(comments: MutableList<CommunityStoryResponseModel.Data.CommunicationStory.Comment>?) {
        mView.recyclerViewComment.adapter = adapter
        comments?.let { adapter.fetchComment(it) }
    }

    private fun initViewPager(images: MutableList<String>) {
        val pagerAdapter = CommunityPagerAdapter(childFragmentManager, images)
        mView.viewPager.offscreenPageLimit = 2
        mView.viewPager.adapter = pagerAdapter
        mView.pageIndicator.attachViewPager(mView.viewPager)
    }

    private fun displayContent(mStory: CommunityStoryResponseModel.Data.CommunicationStory) {
        mView.textTitle.text = mStory.title
        mView.textCommunityContent.text = mStory.description_content
        mView.textCountView.text =
            "${mStory.total_viewed_amount?.toShortStyle()} ${getString(pattararittigul.sasin.unesco_android.R.string.suffix_views)}"
        mView.textDate.text = "${mStory.date_created?.asDate()}"
        mView.textTime.text = "${mStory.date_created?.asTime()}"
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CommunityDetailViewModel::class.java)
        viewModel.storyDisplayedId = story?.story_id
        Log.d(TAG, "storyId: ${story?.story_id}")
        story?.story_id?.let { viewModel.threadViews(it) }
        observeUpdateComment()
        mView.likeCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.handleOnCheckBoxChange(isChecked)
        }
    }

    private fun observeUpdateComment() {
        viewModel.storyResult().observe(viewLifecycleOwner, Observer {
            val comments = it.comments
            val countLike = it.count_like
            Log.d(TAG, "response: $it")
            if (comments != null) {
                val commentUpdatedCount = comments.size - adapter.itemCount
                val newCommentList: MutableList<CommunityStoryResponseModel.Data.CommunicationStory.Comment> =
                    comments.subList(0, commentUpdatedCount)
                newCommentList.asReversed().forEach { comment -> adapter.addNewComment(comment) }
            }
            if (countLike != null) {
                mView.textCountLike.text = "$countLike like"
            }
        })
    }
}
