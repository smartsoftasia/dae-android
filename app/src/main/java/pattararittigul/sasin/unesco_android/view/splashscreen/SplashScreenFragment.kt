package pattararittigul.sasin.unesco_android.view.splashscreen

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.splash_screen_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.finish
import pattararittigul.sasin.unesco_android.extension.toActivity
import pattararittigul.sasin.unesco_android.view.login.LoginActivity
import pattararittigul.sasin.unesco_android.view.main.MainActivity


class SplashScreenFragment : Fragment() {

    private lateinit var mView: View
    private val timer: CountDownTimer? by lazy {
        object : CountDownTimer(3000L, 1000L) {
            override fun onFinish() {
                toNextActivity()
            }

            override fun onTick(p0: Long) = Unit
        }
    }

    companion object {
        fun newInstance() = SplashScreenFragment()
    }

    private lateinit var viewModel: SplashScreenViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.splash_screen_fragment, container, false)
        SharedPreferences.init(requireContext().applicationContext)
        appLogoGlowUp()
        return mView

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SplashScreenViewModel::class.java)
    }

    private fun appLogoGlowUp() {
        val logoAnimation = logoAnimation()
        mView.appLogoSplashScreen.startAnimation(logoAnimation)
    }

    override fun onResume() {
        super.onResume()
        timer?.start()
    }

    override fun onPause() {
        super.onPause()
        timer?.cancel()
    }

    private fun textAnimation(): AlphaAnimation {
        return AlphaAnimation(0f, 1f).apply {
            fillAfter = true
            duration = 1000L
        }.also {
            it.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) = Unit
                override fun onAnimationEnd(p0: Animation?) = Unit

                override fun onAnimationStart(p0: Animation?) {
                    mView.textSplashScreen.visibility = View.VISIBLE
                }
            })
        }
    }

    private fun logoAnimation(): AlphaAnimation {
        val animation = AlphaAnimation(0f, 1f)
        animation.fillAfter = true
        animation.duration = 1000L
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                mView.appLogoSplashScreen.visibility = View.VISIBLE
            }

            override fun onAnimationRepeat(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                mView.textSplashScreen.startAnimation(textAnimation())
            }
        })
        return animation
    }

    private fun toNextActivity() {
        if (viewModel.isUserLoggedIn()) toMainActivity()
        else toLoginActivity()
        finish()
    }

    private fun toLoginActivity() {
        toActivity<LoginActivity>()
    }

    private fun toMainActivity() {
        toActivity<MainActivity>()
    }
}
