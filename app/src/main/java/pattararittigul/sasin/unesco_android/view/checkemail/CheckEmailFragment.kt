package pattararittigul.sasin.unesco_android.view.checkemail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.check_email_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.finish

class CheckEmailFragment : Fragment() {

    private lateinit var mView: View

    companion object {
        fun newInstance() =
            CheckEmailFragment()
    }

    private lateinit var viewModel: CheckEmailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.check_email_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        mView.buttonDismiss.setOnClickListener {
            finish()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CheckEmailViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
