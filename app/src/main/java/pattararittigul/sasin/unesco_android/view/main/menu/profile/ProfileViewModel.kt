package pattararittigul.sasin.unesco_android.view.main.menu.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.executeGetDetail
import pattararittigul.sasin.unesco_android.extension.executeUpdateProfile
import pattararittigul.sasin.unesco_android.model.ProfileModel
import java.io.File

class ProfileViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val profileData = MutableLiveData<ProfileModel.Data>()

    fun updateProfile(
        firstName: String,
        lastName: String,
        hometown: String? = SharedPreferences.getUserDetail().howetown,
        image: File? = null
    ) {
        SharedPreferences.getUserDetail().id?.let {
            executeUpdateProfile(
                id = it,
                firstName = firstName,
                lastName = lastName,
                hometown = hometown,
                profile = image,
                appToken = null
            ).subscribeBy(
                onNext =
                { response ->
                    if (response.status) {
                        executeGetProfile()
                    }
                },
                onError = {}
            ).addTo(compositeDisposable)
        }
    }

    fun executeGetProfile() {
        executeGetDetail("${SharedPreferences.getUserDetail().id}").subscribeBy(
            onNext = {
                val mProfile = it.data
                validateData(mProfile)
            },
            onError = { it.printStackTrace() }
        ).addTo(compositeDisposable)
    }

    fun profileResult(): LiveData<ProfileModel.Data> {
        return profileData
    }

    fun updateDisplayData() {
        val userDetail = SharedPreferences.getUserDetail()
        profileData.postValue(userDetail)
    }

    private fun validateData(profile: ProfileModel.Data?) {
        Log.d(TAG, "validateData : $profile")
        if (profile != null && !profile.id.isNullOrBlank()) {
            profile.let { SharedPreferences.setUserDetail(it) }
        }
        updateDisplayData()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
