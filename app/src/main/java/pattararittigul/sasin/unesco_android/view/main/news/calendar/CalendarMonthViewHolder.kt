package pattararittigul.sasin.unesco_android.view.main.news.calendar

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_new_month.view.*
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel

class CalendarMonthViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setViewData(month: EventCalendarResponseModel.Data.CalendarMonth) {
        val name = "${month.month_name} ${month.year}"
        itemView.monthName.text = name
        val layoutManager = GridLayoutManager(itemView.context, 7)
        val adapter =
            GridDayAdapter(month.days!!)

        itemView.gridDay.layoutManager = layoutManager
        itemView.gridDay.adapter = adapter

    }


}
