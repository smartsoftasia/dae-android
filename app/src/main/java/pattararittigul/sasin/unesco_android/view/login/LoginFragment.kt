package pattararittigul.sasin.unesco_android.view.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.login_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.*
import pattararittigul.sasin.unesco_android.util.LocaleManager
import pattararittigul.sasin.unesco_android.view.BaseActivity
import pattararittigul.sasin.unesco_android.view.forgotpassword.ForgotPasswordActivity
import pattararittigul.sasin.unesco_android.view.main.MainActivity
import pattararittigul.sasin.unesco_android.view.register.RegisterActivity
import java.util.*


class LoginFragment : Fragment(), TextWatcher {

    private lateinit var mView: View
    private val callbackManager by lazy { CallbackManager.Factory.create() }
    private val loading: ProgressDialog by lazy {
        requireContext().loadingDialog.apply {
            setCancelable(false)
        }
    }

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(
            R.layout.login_fragment,
            container,
            false
        )
        FacebookSdk.fullyInitialize()
        initFacebook()
        init()
        return mView
    }

    private fun initFacebook() {
        mView.buttonLoginFacebook.setPermissions(listOf("email"))
        mView.buttonLoginFacebook.fragment = this
        mView.buttonLoginFacebook.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    Log.d(TAG, "==============> FB $result")
                    result?.apply {
                        accessToken?.userId?.let { userId ->
                            viewModel.loginWithFacebook(userId, userId)
                        }
                    }
                }

                override fun onCancel() {
                    Log.d(TAG, "==============> FB cancel")
                }

                override fun onError(error: FacebookException?) {
                    Log.d(TAG, "==============> FB $error")
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun init() {
        mView.buttonFacebookLogin.setOnClickListener {
            FacebookSdk.removeLoggingBehavior(LoggingBehavior.APP_EVENTS)
            FacebookSdk.clearLoggingBehaviors()
            mView.buttonLoginFacebook.performClick()
        }
        mView.buttonLogin.setOnClickListener {
            loading.show()
            performLogin()
        }
        mView.buttonRegister.setOnClickListener {
            toRegisterActivity()
        }
        mView.buttonForgotPassword.setOnClickListener {
            toForgotPassword()
        }
        handleLoginButton()
        mView.inputEmail.addTextChangedListener(this)
        mView.inputPassword.addTextChangedListener(this)

        mView.checkBoxLanguage.isChecked =
            (requireActivity() as BaseActivity).getCurrentLanguage() == Locale(LocaleManager.MYANMAR)

        mView.checkBoxLanguage.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                SharedPreferences.setLanguagePref(LocaleManager.MYANMAR)
                (requireActivity() as BaseActivity).setLanguage(Locale(LocaleManager.MYANMAR))
            } else {
                SharedPreferences.setLanguagePref(LocaleManager.ENGLISH)
                (requireActivity() as BaseActivity).setLanguage(Locale(LocaleManager.ENGLISH))
            }
        }
    }

    private fun performLogin() {
        mView.inputEmail.text.toString().trim().let {
            if (it.isEmailFormat()) {
                viewModel.loginByEmail(it, mView.inputPassword.text.toString())
            } else {
                displayErrorDialog()
            }
        }
    }

    private fun toForgotPassword() {
        toActivity<ForgotPasswordActivity>()
    }

    override fun afterTextChanged(s: Editable?) {
        handleLoginButton()
    }

    override fun beforeTextChanged(
        s: CharSequence?,
        start: Int,
        count: Int,
        after: Int
    ) {// To change body of created functions use File | Settings | File Templates.
    }

    override fun onTextChanged(
        s: CharSequence?,
        start: Int,
        before: Int,
        count: Int
    ) {// To change body of created functions use File | Settings | File Templates.
    }

    private fun handleLoginButton() {
        mView.buttonLogin.isEnabled =
            mView.inputEmail.text.toString().isNotBlank() && mView.inputPassword.text.toString().isNotBlank()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        observed()
    }

    private fun observed() {
        viewModel.loginResult().observe(viewLifecycleOwner, Observer {
            loading.dismiss()
            it?.let {
                toMainActivity()
            } ?: displayErrorDialog()
        })
    }


    private fun toMainActivity() {
        toActivity<MainActivity>()
        finish()
        modalInActivityAnimate()
    }

    private fun toRegisterActivity() {
        toActivity<RegisterActivity>()
    }
}
