package pattararittigul.sasin.unesco_android.view.main.news.calendar

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.executeAllEvent
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel

class CalendarViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val eventCalendarData = MutableLiveData<EventCalendarResponseModel.Data>()


    fun getAllEvent() {
        executeAllEvent()
            .subscribeBy(
                onNext = {
                    Log.d(TAG, "calendar: $it")
                    if (it.status) eventCalendarData.postValue(it.data)
                    else eventCalendarData.postValue(null)
                },
                onError = {
                    eventCalendarData.postValue(null)
                }
            ).addTo(compositeDisposable)
    }


    fun eventCalendarResult(): LiveData<EventCalendarResponseModel.Data> = eventCalendarData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
