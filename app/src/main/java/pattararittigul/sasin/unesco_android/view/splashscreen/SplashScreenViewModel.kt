package pattararittigul.sasin.unesco_android.view.splashscreen

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import pattararittigul.sasin.unesco_android.database.SharedPreferences


class SplashScreenViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    fun isUserLoggedIn(): Boolean {
        return SharedPreferences.isLogin()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
