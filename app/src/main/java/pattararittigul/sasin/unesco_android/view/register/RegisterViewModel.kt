package pattararittigul.sasin.unesco_android.view.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeRegister
import pattararittigul.sasin.unesco_android.model.BaseResponse

class RegisterViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val registerData = MutableLiveData<BaseResponse>()

    fun register(
        name: String,
        lastName: String,
        homeTown: String,
        email: String,
        password: String
    ) {
        executeRegister(
            firstName = name,
            lastName = lastName,
            hometown = homeTown,
            email = email,
            password = password
        ).subscribeBy(
            onNext = {
                if (it.status) {
                    registerData.postValue(it)
                } else {
                    registerData.postValue(BaseResponse(it.message, false))
                }
            },
            onError = {
                registerData.postValue(BaseResponse("${it.localizedMessage}", false))
            }
        ).addTo(compositeDisposable)
    }

    fun registerResult() = registerData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
