package pattararittigul.sasin.unesco_android.view.main.library

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.main_library_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.hideKeyboard
import pattararittigul.sasin.unesco_android.extension.loadingDialog
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel


class MainLibraryFragment : Fragment() {

    private lateinit var mView: View
    private val pagerAdapter by lazy { LibraryPagerAdapter(childFragmentManager, mutableListOf()) }
    private var currentTab = 0
    private val loading: ProgressDialog by lazy { requireContext().loadingDialog }

    companion object {
        fun newInstance() = MainLibraryFragment()
    }

    private lateinit var viewModel: MainLibraryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.main_library_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        setupViewPager()
        setupTabLayout()
        mView.inputSearch.setOnEditorActionListener { v, actionId, event ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    performSearch(mView.inputSearch.text.toString().trim())
                    true
                }
                else -> false
            }
        }
        mView.inputSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    viewModel.allLibrary()
                    loading.show()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })
    }

    private fun performSearch(input: String) {
        viewModel.allLibrary(input)
        hideKeyboard()
        loading.show()
    }


    private fun setupViewPager() {
        mView.viewPager.adapter = pagerAdapter
        mView.viewPager.offscreenPageLimit = 3
    }

    private fun createTab(category: String): TabLayout.Tab {
        return mView.tabLayout.newTab()
            .also { it.text = category }
    }

    @SuppressLint("NewApi")
    private fun updateTab(categories: MutableList<LibraryAllResponseModel.Data.Category>) {
        mView.tabLayout.removeAllTabs()
        val displayList = prepareLibraryList(categories)
        pagerAdapter.updateTabs(displayList)
        displayList.forEach {
            mView.tabLayout.addTab(createTab("${it.category_name}"))
        }
        mView.tabLayout.setupWithViewPager(mView.viewPager)
        pagerAdapter.notifyDataSetChanged()
    }

    private fun prepareLibraryList(categories: MutableList<LibraryAllResponseModel.Data.Category>): MutableList<LibraryAllResponseModel.Data.Category> {
        val categoryAll = LibraryAllResponseModel.Data.Category(
            category_name = "All",
            books = categories.toAllBookCollection()
        )
        val displayCategories: MutableList<LibraryAllResponseModel.Data.Category> = mutableListOf()
        displayCategories.addAll(categories)
        displayCategories.add(0, categoryAll)
        return displayCategories
    }

    private fun MutableList<LibraryAllResponseModel.Data.Category>.toAllBookCollection(): MutableList<LibraryAllResponseModel.Data.Category.Book> {
        val list = mutableListOf<LibraryAllResponseModel.Data.Category.Book>()
        this.forEach {
            list.addAll(it.books ?: return@forEach)
        }
        return list
    }

    private fun setupTabLayout() {
        mView.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) = Unit

            override fun onTabUnselected(tab: TabLayout.Tab?) = Unit

            override fun onTabSelected(tab: TabLayout.Tab) {
                currentTab = mView.viewPager.currentItem
                mView.viewPager.setCurrentItem(currentTab, true)
                hideKeyboard()
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainLibraryViewModel::class.java)
        viewModel.allLibrary()
        loading.show()
        observeResult()
    }

    private fun observeResult() {
        viewModel.getLibraryResult().observe(viewLifecycleOwner, Observer {
            loading.dismiss()
            it?.categories?.let { categories ->
                updateTab(categories)
            }
        })

    }


}
