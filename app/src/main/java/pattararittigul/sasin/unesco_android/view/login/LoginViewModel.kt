package pattararittigul.sasin.unesco_android.view.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.executeLogin
import pattararittigul.sasin.unesco_android.extension.executeLoginFacebook
import pattararittigul.sasin.unesco_android.model.ProfileModel

class LoginViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val loginData = MutableLiveData<ProfileModel.Data>()

    fun loginByEmail(email: String, password: String) {
        executeLogin(email, password).subscribeBy(
            onNext = {
                prepareToLogin(it.data)
            },
            onError = {
                loginData.postValue(null)
            }
        ).addTo(compositeDisposable)
    }

    fun loginWithFacebook(facebookId: String, token: String) {
        executeLoginFacebook(facebookId, token)
            .subscribeBy(
                onNext = {
                    if (it.status) {
                        prepareToLogin(it.data)
                    }
                },
                onError = {
                    loginData.postValue(null)
                }
            )
            .addTo(compositeDisposable)
    }

    fun prepareToLogin(data: ProfileModel.Data?) {
        data?.let {
            SharedPreferences.setUserDetail(it)
            SharedPreferences.setLogin(true)
            loginData.postValue(data)
        }
    }


    fun loginResult() = loginData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
