package pattararittigul.sasin.unesco_android.view.main.menu.contactus

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeContactUs
import pattararittigul.sasin.unesco_android.model.ContactUsResponseModel

class ContactUsViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val contactData = MutableLiveData<ContactUsResponseModel.Data>()
    var contact: Triple<String?, String?, String?> = Triple("", "", "")


    fun getContactUs() {
        executeContactUs()
            .subscribeBy(
                onNext = {
                    if (it.status) {
                        val response = it.data
                        contactData.postValue(response)
                        contact = Triple(
                            response?.email?.email_address,
                            response?.phone?.phone_number,
                            response?.website?.website_url
                        )
                    } else {
                        contactData.postValue(null)
                    }
                },
                onError = {
                    contactData.postValue(null)
                })
            .addTo(compositeDisposable)
    }

    fun contactUsResult(): LiveData<ContactUsResponseModel.Data> = contactData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun getPhoneNumber() = "08103158432"
    fun getEmail() = "sasin.ping@gmail.com"
    fun getWebsiteUrl() = "https://www.google.com"
}
