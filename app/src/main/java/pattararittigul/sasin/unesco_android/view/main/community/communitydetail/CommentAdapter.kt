package pattararittigul.sasin.unesco_android.view.main.community.communitydetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

class CommentAdapter(private val commentList: MutableList<CommunityStoryResponseModel.Data.CommunicationStory.Comment>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentViewHolder(view)
    }

    override fun getItemCount(): Int = commentList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CommentViewHolder) {
            val comment = commentList[position]
            holder.setupView(comment)
        }
    }

    fun addNewComment(comment: CommunityStoryResponseModel.Data.CommunicationStory.Comment) {
        commentList.add(0, comment)
        notifyItemInserted(0)
    }

    fun fetchComment(comments: MutableList<CommunityStoryResponseModel.Data.CommunicationStory.Comment>) {
        commentList.clear()
        commentList.addAll(comments)
        notifyDataSetChanged()
    }
}