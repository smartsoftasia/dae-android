package pattararittigul.sasin.unesco_android.view.main.menu

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.main_menu_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.displayDialog
import pattararittigul.sasin.unesco_android.extension.modalInActivityAnimate
import pattararittigul.sasin.unesco_android.extension.modalOutActivityAnimate
import pattararittigul.sasin.unesco_android.extension.toActivity
import pattararittigul.sasin.unesco_android.view.login.LoginActivity
import pattararittigul.sasin.unesco_android.view.main.menu.changelanguage.ChangeLanguageActivity
import pattararittigul.sasin.unesco_android.view.main.menu.contactus.ContactUsActivity
import pattararittigul.sasin.unesco_android.view.main.menu.directory.DirectoryActivity
import pattararittigul.sasin.unesco_android.view.main.menu.profile.ProfileActivity
import pattararittigul.sasin.unesco_android.view.tutorial.TutorialActivity

class MainMenuFragment : Fragment() {

    private lateinit var mView: View

    companion object {
        fun newInstance() = MainMenuFragment()
    }

    private lateinit var viewModel: MainMenuViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.main_menu_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        mView.buttonClassmap.setOnClickListener {
           openClassMapWeb()
        }
        mView.buttonTutorial.setOnClickListener {
            toActivity<TutorialActivity>()
            modalInActivityAnimate()
        }
        mView.buttonProfile.setOnClickListener {
            toActivity<ProfileActivity>()
        }

        mView.buttonBuildingCourse.setOnClickListener {
            openBuildingCourseWeb()
        }

        mView.buttonConnect.setOnClickListener {
            toActivity<DirectoryActivity>()
        }

        mView.buttonContactUs.setOnClickListener {
            toActivity<ContactUsActivity>()
        }

        mView.buttonLogout.setOnClickListener {
            displayDialog(
                R.string.logout,
                R.string.logout_dialog_desc,
                R.string.logout,
                DialogInterface.OnClickListener { dialog, _ ->
                    logout()
                    dialog.dismiss()
                },
                R.string.cancel, DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                }, false
            )
        }

        mView.buttonChangeLanguage.setOnClickListener {
            toActivity<ChangeLanguageActivity>()
        }
    }

    private fun openBuildingCourseWeb() {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri = Uri.parse("https://lll-olc.net/")
        intent.data = uri
        startActivity(intent)
    }


    private fun openClassMapWeb() {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri = Uri.parse("https://play.google.com/store/apps/details?id=org.unesco.classmap&hl=en/")
        intent.data = uri
        startActivity(intent)
    }


    private fun logout() {
        toActivity<LoginActivity>()
        FacebookSdk.clearLoggingBehaviors()
        LoginManager.getInstance().logOut()
        requireActivity().finishAffinity()
        modalOutActivityAnimate()
        SharedPreferences.clearPreference()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainMenuViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
