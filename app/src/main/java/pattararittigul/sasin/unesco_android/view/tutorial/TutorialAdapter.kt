package pattararittigul.sasin.unesco_android.view.tutorial

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.enum.TutorialItemStyle
import pattararittigul.sasin.unesco_android.model.TutorialItemModel

class TutorialAdapter(private val items: MutableList<TutorialItemModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val viewLeft =
            LayoutInflater.from(parent.context).inflate(R.layout.item_tutorial_left, parent, false)
        val viewRight =
            LayoutInflater.from(parent.context).inflate(R.layout.item_tutorial_right, parent, false)

        return if (viewType == TutorialItemStyle.LEFT.type) {
            TutorialViewHolder(viewLeft)
        } else {
            TutorialViewHolder(viewRight)
        }
    }


    override fun getItemViewType(position: Int): Int {
        return if (position % 2 == 0) TutorialItemStyle.LEFT.type else TutorialItemStyle.RIGHT.type
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (holder is TutorialViewHolder) {
            val item = items[position]
            holder.setupView(item)
        }
    }

    fun updateList(list: MutableList<TutorialItemModel>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }
}
