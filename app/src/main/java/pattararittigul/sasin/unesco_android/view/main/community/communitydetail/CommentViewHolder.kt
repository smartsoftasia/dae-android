package pattararittigul.sasin.unesco_android.view.main.community.communitydetail

import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_comment.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.asDateTime
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

class CommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun setupView(comment: CommunityStoryResponseModel.Data.CommunicationStory.Comment) {

        itemView.textCommenterName.text = comment.writer_name
        itemView.textCommentTime.text = comment.date_created?.asDateTime()
        itemView.textCommentContent.text = comment.comment_content
        GlideApp.with(itemView).load(Uri.parse(comment.writer_image))
            .circleCrop()
            .placeholder(R.drawable.place_holder_profile_small)
            .error(R.drawable.place_holder_profile_small)
            .into(itemView.imageProfile)
    }
}
