package pattararittigul.sasin.unesco_android.view.forgotpassword

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.forgot_password_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.displayErrorDialog
import pattararittigul.sasin.unesco_android.extension.finish
import pattararittigul.sasin.unesco_android.extension.hideKeyboard
import pattararittigul.sasin.unesco_android.extension.toActivity
import pattararittigul.sasin.unesco_android.view.checkemail.CheckEmailActivity

class ForgotPasswordFragment : Fragment() {

    private lateinit var mView: View

    companion object {
        fun newInstance() =
            ForgotPasswordFragment()
    }

    private lateinit var viewModel: ForgotPasswordViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.forgot_password_fragment, container, false)
        init()
        return mView
    }

    private fun updateButton() {
        mView.buttonSend.isEnabled = mView.inputEmail.text.toString().isNotBlank()
    }

    private fun init() {
        mView.buttonSend.setOnClickListener {
            viewModel.sendForgotPasswordRequest(mView.inputEmail.text.toString())
        }
        mView.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
            hideKeyboard()
        }
        mView.inputEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                updateButton()
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {// To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {// To change body of created functions use File | Settings | File Templates.
            }
        })
    }

    private fun toCheckEmailActivity() {
        toActivity<CheckEmailActivity>()
        finish()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)
        viewModel.forgotPasswordResult().observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.status) toCheckEmailActivity()
                else displayErrorDialog(message = it.message)
            } ?: displayErrorDialog()
        })
    }

}
