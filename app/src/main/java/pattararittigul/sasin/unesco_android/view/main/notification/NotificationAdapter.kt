package pattararittigul.sasin.unesco_android.view.main.notification

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.NotificationModel

class NotificationAdapter(val notificationList: MutableList<NotificationModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return NotificationViewHolder(view)
    }

    override fun getItemCount(): Int = notificationList.size

    fun updateNotificationList(list: MutableList<NotificationModel>) {
        notificationList.clear()
        notificationList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (holder is NotificationViewHolder) {
            val notification = notificationList[position]
            holder.setupView(notification)
        }
    }
}
