package pattararittigul.sasin.unesco_android.view.checkemail

import android.os.Bundle
import androidx.core.content.ContextCompat
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.setStatusBarColor
import pattararittigul.sasin.unesco_android.view.BaseActivity

class CheckEmailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.check_email_activity)
        setStatusBarColor(ContextCompat.getColor(this, R.color.color_primary_variant))
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CheckEmailFragment.newInstance())
                .commitNow()
        }
    }

}
