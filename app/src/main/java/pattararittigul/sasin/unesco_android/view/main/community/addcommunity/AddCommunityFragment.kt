package pattararittigul.sasin.unesco_android.view.main.community.addcommunity

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.add_community_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class AddCommunityFragment : Fragment(), TextWatcher {

    private lateinit var mView: View
    //    private val uploadImageFilePathList = mutableListOf<String>()
    private val adapter = ImageUploadedAdapter(mutableListOf())
    private var selectedImage: File? = null

    private val loading: ProgressDialog by lazy {
        requireContext().loadingDialog.apply {
            setCancelable(false)
        }
    }

    companion object {
        private const val EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE = 556
        private const val REQUEST_CAMERA_CAPTURE = 431
        private const val EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 436
        private const val REQUEST_GALLERY_PHOTO = 499

        fun newInstance(): AddCommunityFragment {
            val fragment = AddCommunityFragment()
            return fragment
        }
    }

    private lateinit var viewModel: AddCommunityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(
            R.layout.add_community_fragment,
            container,
            false
        )
        init()
        return mView
    }

    private fun init() {
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        mView.recyclerViewImageAdded.layoutManager = layoutManager
        mView.recyclerViewImageAdded.setHasFixedSize(false)
        mView.recyclerViewImageAdded.adapter = adapter
        mView.inputTitle.addTextChangedListener(this)
        mView.inputDetail.addTextChangedListener(this)

        mView.toolbar.setNavigationOnClickListener {
            val returnIntent = Intent()
            requireActivity().setResult(Activity.RESULT_CANCELED, returnIntent)
            requireActivity().onBackPressed()
            hideKeyboard()
        }
        mView.buttonAddImage.setOnClickListener {
            val items = arrayOf(
                getString(R.string.camera),
                getString(R.string.gallery),
                getString(R.string.cancel)
            )
            displayItemDialog(items)
        }


        adapter.setListener(object : CommunityImageListener {
            override fun clickReselect(imagePath: String, position: Int) {
                viewModel.uploadImageFilePathList.removeAt(position)
                prepareToDispatchGallery()
            }
        })
        mView.buttonCreate.setOnClickListener {
            loading.show()
            viewModel.createStory(
                mView.inputTitle.text.toString(),
                mView.inputDetail.text.toString()
            )
        }
    }

    private fun displayItemDialog(buttons: Array<String>) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setItems(buttons) { dialog, which ->
            when (buttons[which]) {
                buttons[0] -> {
                    prepareToDispatchTakePicture()
                    dialog.dismiss()
                }
                buttons[1] -> {
                    prepareToDispatchGallery()
                    dialog.dismiss()
                }
                buttons[2] -> {
                    dialog.dismiss()
                }
            }
        }.also { it.show() }
    }

    override fun afterTextChanged(s: Editable?) {
        updateCreateButton()
    }

    private fun updateCreateButton() {
        mView.buttonCreate.isEnabled =
            mView.inputTitle.text.toString().isNotBlank()
                    && mView.inputDetail.text.toString().isNotBlank()
    }

    override fun beforeTextChanged(
        s: CharSequence?,
        start: Int,
        count: Int,
        after: Int
    ) {// To change body of created functions use File | Settings | File Templates.
    }

    override fun onTextChanged(
        s: CharSequence?,
        start: Int,
        before: Int,
        count: Int
    ) {// To change body of created functions use File | Settings | File Templates.
    }

    private fun backToMainCommunity() {
        val returnIntent = Intent()
        requireActivity().setResult(RESULT_OK, returnIntent)
        finish()
    }

    private fun prepareToDispatchGallery() {
        if (externalStorageIsNotGrant()) {
            requestExternalStoragePermission()
        } else {
            dispatchGalleryIntent()
        }
    }

    private fun externalStorageIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED

    private fun requestExternalStoragePermission() {
        requestPermissions(
            arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE
        )
    }

    private fun requestExternalStorageAndCameraPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
            EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(
            TAG,
            "image onActivityResult resultCode: $resultCode, requestCode: $requestCode, data: $data"
        )
        when (requestCode) {
            REQUEST_GALLERY_PHOTO -> {
                if (resultCode == RESULT_OK) {
                    data?.data?.let {
                        try {
                            val fos: FileOutputStream?
                            val photoFile: File?
                            photoFile = createImageFile()
                            fos = FileOutputStream(photoFile)
                            getBitmapFromUri(it).compress(Bitmap.CompressFormat.JPEG, 50, fos)
                            fos.close()
                            selectedImage = photoFile
                        } catch (e: Exception) {
                            Log.e("SAVE_IMAGE", e.message, e)
                        }
                    }.also {
                        Log.d(TAG, "image on activity result: $it")
                        selectedImage?.let { prepareUploadedImages() }
                    }
                }
            }

            REQUEST_CAMERA_CAPTURE -> {
                if (resultCode == RESULT_OK) {
                    selectedImage?.let { prepareUploadedImages() }

                }
            }
        }
    }

    private fun prepareUploadedImages() {
        val filePath = selectedImage?.absolutePath
        filePath?.let {
            viewModel.uploadImageFilePathList.add(0, it)
            updateCreateButton()
            adapter.updateImages(viewModel.uploadImageFilePathList)
        }
        setEnableAddImage()
    }

    private fun setEnableAddImage() {
        when (viewModel.uploadImageFilePathList.size) {
            0 -> {
                mView.buttonAddImage.visibility = View.VISIBLE
                mView.recyclerViewImageAdded.visibility = View.GONE
            }
            1, 2 -> {
                mView.buttonAddImage.visibility = View.VISIBLE
                mView.recyclerViewImageAdded.visibility = View.VISIBLE
            }
            3 -> {
                mView.buttonAddImage.visibility = View.GONE
                mView.recyclerViewImageAdded.visibility = View.VISIBLE
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchGalleryIntent()
                }
                return
            }
            EXTERNAL_AND_CAMERA_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent()
                }
                return
            }
        }
    }


    private fun dispatchGalleryIntent() {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO)
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (photoFile != null) {
                val photoUri =
                    requireContext().getFileUri(photoFile)
                selectedImage = photoFile
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                this.startActivityForResult(takePictureIntent, REQUEST_CAMERA_CAPTURE)
            }
        }
    }

    private fun prepareToDispatchTakePicture() {
        if (cameraAndExternalStorageIsNotGrant()) {
            requestExternalStorageAndCameraPermission()
        } else {
            dispatchTakePictureIntent()
        }
    }

    private fun cameraAndExternalStorageIsNotGrant() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddCommunityViewModel::class.java)
        viewModel.createResult().observe(viewLifecycleOwner, Observer {
            loading.hide()
            it?.let {
                backToMainCommunity()
            } ?: displayErrorDialog()
        })
    }
}


