package pattararittigul.sasin.unesco_android.view.login

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.core.content.ContextCompat
import pattararittigul.sasin.unesco_android.extension.setStatusBarColor
import pattararittigul.sasin.unesco_android.view.BaseActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class LoginActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(pattararittigul.sasin.unesco_android.R.layout.login_activity)
        setStatusBarColor(
            ContextCompat.getColor(
                this,
                pattararittigul.sasin.unesco_android.R.color.color_primary_variant
            )
        )
        checkKeyHash()
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    pattararittigul.sasin.unesco_android.R.id.container,
                    LoginFragment.newInstance(), "LoginFragment"
                )
                .commitNow()
        }

    }

    private fun checkKeyHash() {
        try {
            val info = packageManager.getPackageInfo(
                "pattararittigul.sasin.unesco_android",
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }

    }
}
