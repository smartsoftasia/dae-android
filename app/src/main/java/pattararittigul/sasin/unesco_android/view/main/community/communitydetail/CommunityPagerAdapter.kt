package pattararittigul.sasin.unesco_android.view.main.community.communitydetail

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class CommunityPagerAdapter(
    fragmentManager: FragmentManager,
    private val images: MutableList<String>
) :
    FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment =
        StoryImageFragment.newInstance(images[position])

    override fun getCount(): Int = images.size

}