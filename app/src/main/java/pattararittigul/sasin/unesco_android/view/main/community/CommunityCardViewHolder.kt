package pattararittigul.sasin.unesco_android.view.main.community

import android.annotation.SuppressLint
import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_community_card.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.asDate
import pattararittigul.sasin.unesco_android.extension.toShortStyle
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

class CommunityCardViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("CheckResult")
    fun setupView(story: CommunityStoryResponseModel.Data.CommunicationStory) {
        itemView.textTitle.text = story.title
        itemView.textDescription.text = story.description_content
        itemView.textCountComment.text =
            "${story?.comments?.size} ${itemView.context.getString(R.string.suffix_comments)}"
        itemView.textDate.text = story.date_created?.asDate()
        itemView.textCountView.text =
            "${story.total_viewed_amount?.toShortStyle()} ${itemView.context.getString(R.string.suffix_views)}"
        if (story.images?.isNotEmpty() == true) {
            GlideApp.with(itemView.context).load(
                Uri.parse(story.images?.get(0))
            )
                .placeholder(R.drawable.place_holder_image_view)
                .error(R.drawable.place_holder_image_view)
                .into(itemView.imageCommunity)
        }
    }
}
