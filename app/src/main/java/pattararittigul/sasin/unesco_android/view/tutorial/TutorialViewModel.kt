package pattararittigul.sasin.unesco_android.view.tutorial

import androidx.lifecycle.ViewModel
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.enum.TutorialItemStyle
import pattararittigul.sasin.unesco_android.model.TutorialItemModel

class TutorialViewModel : ViewModel() {

    private val tutorialItemList = mutableListOf<TutorialItemModel>(
        TutorialItemModel(
            R.drawable.tutorial_news_event,
            R.string.news_tutorial_title,
            R.string.news_tutorial_desc,
            TutorialItemStyle.LEFT
        ),
        TutorialItemModel(
            R.drawable.tutorial_community,
            R.string.community_tutorial_title,
            R.string.community_tutorial_desc,
            TutorialItemStyle.RIGHT
        ),
        TutorialItemModel(
            R.drawable.tutorial_library,
            R.string.library_tutorial_title,
            R.string.library_tutorial_desc,
            TutorialItemStyle.LEFT
        ),
        TutorialItemModel(
            R.drawable.tutorial_notification,
            R.string.notification_tutorial_title,
            R.string.notification_tutorial_desc,
            TutorialItemStyle.RIGHT
        ),
        TutorialItemModel(
            R.drawable.tutorial_menu,
            R.string.menu_tutorial_title,
            R.string.menu_tutorial_desc,
            TutorialItemStyle.LEFT
        )
    )

    fun getTutorialList() = tutorialItemList

}
