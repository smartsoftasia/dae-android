package pattararittigul.sasin.unesco_android.view.main.library

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeLibraryAll
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel

class MainLibraryViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val libraryData = MutableLiveData<LibraryAllResponseModel.Data>()


    fun allLibrary(input: String = "") {
        executeLibraryAll()
            .subscribeBy(
                onNext = {
                    if (input.isNotBlank()) {
                        searchResult(input, it.data)
                    } else {
                        libraryData.postValue(it.data)
                    }
                },
                onError = {
                    libraryData.postValue(null)
                    it.printStackTrace()
                })
            .addTo(compositeDisposable)
    }

    private fun searchResult(input: String, data: LibraryAllResponseModel.Data?) {
        val books = data?.categories
        val searchList: MutableList<LibraryAllResponseModel.Data.Category> = mutableListOf()
        books?.forEach {
            val searchBooks =
                it.books?.filter { book -> book.title?.contains(input, true) == true }
                    ?.toMutableList()
            searchList.add(it.copy(books = searchBooks))
        }
        libraryData.postValue(LibraryAllResponseModel.Data(searchList))
    }

    fun getLibraryResult(): LiveData<LibraryAllResponseModel.Data> = libraryData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
