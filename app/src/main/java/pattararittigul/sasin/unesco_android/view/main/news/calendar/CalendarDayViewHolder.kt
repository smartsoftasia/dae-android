package pattararittigul.sasin.unesco_android.view.main.news.calendar

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.adapter_new_day.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel
import kotlin.math.absoluteValue

class CalendarDayViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setViewData(day: EventCalendarResponseModel.Data.CalendarMonth.Day) {
        setDisplayDate(day)
        setHasEvent(day)
        setIsToday(day)
        setEventViewOnClick(day)
    }

    private fun setEventViewOnClick(day: EventCalendarResponseModel.Data.CalendarMonth.Day) {
        itemView.setOnClickListener {
            if (!day.events.isNullOrEmpty()) {
                onDaySelected(day)
            }
        }
    }

    private fun setIsToday(day: EventCalendarResponseModel.Data.CalendarMonth.Day) {
        day.is_today?.let {
            when {
                it -> itemView.dateDisplay.background =
                    ContextCompat.getDrawable(itemView.context, R.drawable.background_date_today)
                else -> itemView.dateDisplay.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        android.R.color.transparent
                    )
                )
            }
        }
    }

    private fun setHasEvent(day: EventCalendarResponseModel.Data.CalendarMonth.Day) {
        day.events?.let {
            val isHasEvent = it.isNotEmpty()
            when {
                isHasEvent -> itemView.eventDot.visibility = View.VISIBLE
                else -> itemView.eventDot.visibility = View.GONE
            }
            itemView.isClickable = isHasEvent
        }
    }

    private fun setDisplayDate(day: EventCalendarResponseModel.Data.CalendarMonth.Day) {
        day.day_of_month?.let { dayOfMonth ->
            day.date?.let { date ->
                when {
                    !(dayOfMonth == 0 && date == "0") -> day.events?.let { event ->
                        val hasEvent = event.isNotEmpty()
                        weekendColor(day)
                        when {
                            hasEvent -> {
                                itemView.dateDisplay.setText(buildDateAsBold(formatDate(date)))
                                itemView.isClickable = true
                                itemView.eventDot.visibility = View.VISIBLE
                            }
                            else -> {
                                itemView.dateDisplay.text = formatDate(date)
                                itemView.isClickable = false
                                itemView.eventDot.visibility = View.INVISIBLE
                            }
                        }
                    }
                    else -> itemView.dateDisplay.text = ""
                }
            }
        }
    }

    private fun weekendColor(day: EventCalendarResponseModel.Data.CalendarMonth.Day) {
        if (day.day_of_month == 0 || day.day_of_month == 6) {
            itemView.dateDisplay.setTextColor(
                ContextCompat.getColor(
                    itemView.context,
                    R.color.color_white_opacity_50
                )
            )
        }
    }

    private fun formatDate(date: String): String {
        return date.toIntOrNull()?.absoluteValue?.toString() ?: date
    }

    private fun onDaySelected(day: EventCalendarResponseModel.Data.CalendarMonth.Day) {
        val sheetDialog = BottomSheetDialog(itemView.context)
        val sheetView: View = (itemView.context as AppCompatActivity).layoutInflater.inflate(
            R.layout.bottom_sheet_events,
            null
        )

        val recyclerViewEvent = sheetView.findViewById<RecyclerView>(R.id.recyclerViewEvent)
        sheetDialog.setContentView(sheetView)
        recyclerViewEvent.layoutManager = LinearLayoutManager(itemView.context)
        recyclerViewEvent.adapter = day.events?.let {
            DayEventAdapter(
                it
            )
        }
        sheetDialog.show()
    }

    private fun buildDateAsBold(date: String): SpannableStringBuilder {
        val stringBuilder = SpannableStringBuilder(date)
        stringBuilder.setSpan(
            android.text.style.StyleSpan(android.graphics.Typeface.BOLD),
            0,
            date.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return stringBuilder
    }


}
