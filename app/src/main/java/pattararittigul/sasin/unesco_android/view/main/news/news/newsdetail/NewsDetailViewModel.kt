package pattararittigul.sasin.unesco_android.view.main.news.news.newsdetail

import android.util.Log
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.executeViewNews

class NewsDetailViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()


    fun newsViews(newsId: String){
        executeViewNews(newsId).
                subscribeBy({
                    Log.d(TAG,"")
                },{}).addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
