package pattararittigul.sasin.unesco_android.view.main.community

import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.main_community_fragment.view.*
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.component.CoverFlowPageTransformer
import pattararittigul.sasin.unesco_android.extension.*
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel
import pattararittigul.sasin.unesco_android.view.main.community.addcommunity.AddCommunityActivity
import pattararittigul.sasin.unesco_android.view.main.community.communitydetail.CommunityDetailActivity

class MainCommunityFragment : Fragment() {

    private lateinit var mView: View
    private val adapter = CommunityAdapter(mutableListOf())

    private val loading: ProgressDialog by lazy {
        requireContext().loadingDialog.apply {
            setCancelable(false)
        }
    }


    companion object {
        private const val ADD_COMMUNITY_REQUEST_CODE = 989
        private const val COMMUNITY_DETAIL_REQUEST_CODE = 887
        fun newInstance() =
            MainCommunityFragment()
    }

    private lateinit var viewModel: MainCommunityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.main_community_fragment, container, false)
        init()
        return mView
    }

    private fun init() {
        setupRecyclerView()
        mView.addButtonFab.setOnClickListener {
            toActivityForResult<AddCommunityActivity>(ADD_COMMUNITY_REQUEST_CODE)
        }
        mView.inputSearch.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    performSearch(mView.inputSearch.text.toString().trim())
                    true
                }
                else -> false
            }
        }

        mView.swipeRefresh.setOnRefreshListener {
            viewModel.feedThread()
        }

        mView.inputSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    viewModel.feedThread()
                    loading.show()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })
    }

    private fun performSearch(input: String) {
        if (input.isNotBlank()) {
            viewModel.feedThread(input)
        } else {
            viewModel.feedThread()
        }
        hideKeyboard()
        loading.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(TAG, "$requestCode, $resultCode, $data")
        when (requestCode) {
            ADD_COMMUNITY_REQUEST_CODE -> {
                if (resultCode == RESULT_OK) {
                    viewModel.feedThread()
                    loading.show()
                }
            }
            COMMUNITY_DETAIL_REQUEST_CODE -> {
                if (resultCode == RESULT_OK) {
                    viewModel.feedThread()
                    loading.show()
                }
            }
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        mView.recyclerViewCommunity.layoutManager = layoutManager
        mView.recyclerViewCommunity.setHasFixedSize(true)
        mView.recyclerViewCommunity.adapter = adapter
        adapter.setListener(object : CommunityCardListener {
            override fun clicked(story: CommunityStoryResponseModel.Data.CommunicationStory) {
                toActivityToResultWith<CommunityDetailActivity>(
                    "COMMUNITY_STORY",
                    story,
                    COMMUNITY_DETAIL_REQUEST_CODE
                )
            }
        })
    }

    private fun setupViewPager(pagerAdapter: PagerAdapter) {
        mView.viewPagerRecommend.apply {
            setPageTransformer(true, CoverFlowPageTransformer(false))
            clipChildren = false
            pageMargin = 40
        }.also {
            it?.adapter = pagerAdapter
        }
        mView.recommendIndicator.attachViewPager(mView.viewPagerRecommend)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainCommunityViewModel::class.java)
        viewModel.feedThread()
        observeThread()
        loading.show()
    }

    private fun observeThread() {
        viewModel.threadResult().observe(viewLifecycleOwner, Observer {
            mView.swipeRefresh.isRefreshing = false
            loading.dismiss()
            it?.let { data ->
                adapter.updateCommunityList(data.communication_stories)
                refreshViewPager(data.communication_stories)
            }
        })

    }

    private fun refreshViewPager(items: MutableList<CommunityStoryResponseModel.Data.CommunicationStory>) {
        items.filter { communicationStory -> communicationStory.is_recommend == true }.let {
            if (it.size > 4) {
                it.subList(0, 3)
            } else {
                it
            }
        }.also {
            if (it.isNotEmpty()) {
                mView.viewPagerRecommend.visibility = View.VISIBLE
                mView.recommendIndicator.visibility = View.VISIBLE
                mView.textHeader.visibility = View.VISIBLE
                val pagerAdapter = RecommendPagerAdapter(
                    childFragmentManager,
                    it.toMutableList()
                )
                setupViewPager(pagerAdapter)
            }
        }
    }

}
