package pattararittigul.sasin.unesco_android.view.main.community.addcommunity

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_image_added.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R
import java.io.File

class ImageUploadedViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setupView(imageFile: String) {
        GlideApp.with(itemView.context)
            .load(File(imageFile))
            .centerCrop()
            .placeholder(R.drawable.place_holder_image_view)
            .error(R.drawable.place_holder_image_view)
            .into(itemView.imageAdded)
    }


}
