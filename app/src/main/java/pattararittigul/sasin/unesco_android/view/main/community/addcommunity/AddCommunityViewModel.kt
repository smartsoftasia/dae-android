package pattararittigul.sasin.unesco_android.view.main.community.addcommunity

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.executeAddStory
import pattararittigul.sasin.unesco_android.extension.executeUploadImage
import pattararittigul.sasin.unesco_android.model.BaseResponse
import pattararittigul.sasin.unesco_android.model.UploadImageResponse
import java.io.File

class AddCommunityViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val uploadImageFilePathList = mutableListOf<String>()
    private val createStoryData = MutableLiveData<BaseResponse>()


    fun createStory(title: String, detail: String) {
        if (uploadImageFilePathList.isNotEmpty()) {
            uploadImage(title, detail)
        } else {
            pushStory(null, title, detail)
        }
    }

    private fun uploadImage(title: String, detail: String) {
        uploadStreamObservable()
            ?.subscribeBy(
                onSuccess = { mutableList: MutableList<UploadImageResponse>? ->
                    val urls = mutableList?.map { it.data }
                    pushStory(urls.toString(), title, detail)
                },
                onError = { Log.e(TAG, "${it.printStackTrace()}") }
            )
            ?.addTo(compositeDisposable)
    }

    private fun uploadStreamObservable(): Single<MutableList<UploadImageResponse>>? {
        return uploadImageFilePathList.map { File(it) }.map { executeUploadImage(it) }
            .reduce { acc, observable ->
                acc.concatWith(observable)
            }.toList()
    }

    private fun pushStory(images: String?, title: String, detail: String) {
        executeAddStory(
            "${SharedPreferences.getUserDetail().id}",
            images,
            title,
            detail
        ).subscribeBy(
            onNext = { createStoryData.postValue(it) },
            onError = { createStoryData.postValue(null) })
            .addTo(compositeDisposable)
    }

    fun createResult(): LiveData<BaseResponse> = createStoryData


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
