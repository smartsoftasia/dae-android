package pattararittigul.sasin.unesco_android.view.main.news.news

import pattararittigul.sasin.unesco_android.model.NewsResponseModel

interface NewsEventListener {
    fun newsClick(news: NewsResponseModel.Data.NewsStory)
}
