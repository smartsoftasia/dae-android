package pattararittigul.sasin.unesco_android.view.main.news.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.model.NewsResponseModel

class NewsFeedAdapter(private val newsList: MutableList<NewsResponseModel.Data.NewsStory>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var callback: NewsEventListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_news_card, parent, false)
        return NewsCardViewHolder(view)
    }

    override fun getItemCount(): Int = newsList.size

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (holder is NewsCardViewHolder) {
            val news = newsList[position]
            holder.setupView(news)

            holder.itemView.setOnClickListener {
                callback?.newsClick(news)
            }
        }
    }

    fun updateNews(news: MutableList<NewsResponseModel.Data.NewsStory>) {
        newsList.clear()
        newsList.addAll(news)
        notifyDataSetChanged()
    }

    fun setListener(listener: NewsEventListener) {
        callback = listener
    }

}
