package pattararittigul.sasin.unesco_android.view.main.community.addcommunity

interface CommunityImageListener {
    fun clickReselect(imagePath: String, position: Int)
}