package pattararittigul.sasin.unesco_android.view.main.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeNotificationAll
import pattararittigul.sasin.unesco_android.model.NotificationAllResponseModel

class MainNotificationViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val notificationData = MutableLiveData<NotificationAllResponseModel.Data>()

    fun getAllNotification(input: String = "") {
        executeNotificationAll()
            .subscribeBy(
                onNext = {
                    if (input.isNotBlank()) {
                        searchResult(input, it.data)
                    } else {
                        if (it.status) notificationData.postValue(it.data)
                        else notificationData.postValue(null)
                    }
                },
                onError = {
                    notificationData.postValue(null)
                    it.printStackTrace()
                })
            .addTo(compositeDisposable)
    }

    private fun searchResult(input: String, data: NotificationAllResponseModel.Data?) {
        val notifications = data?.notifications
        val searchList =
            notifications?.filter { it.title?.contains(input, true) == true }?.toMutableList()
        searchList?.let { notificationData.postValue(NotificationAllResponseModel.Data(it)) }
    }

    fun notificationResult(): LiveData<NotificationAllResponseModel.Data> = notificationData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
