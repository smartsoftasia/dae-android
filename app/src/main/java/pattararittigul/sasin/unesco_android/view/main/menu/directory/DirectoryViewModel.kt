package pattararittigul.sasin.unesco_android.view.main.menu.directory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeDirectoryAll
import pattararittigul.sasin.unesco_android.model.AllDirectoryResponseModel

class DirectoryViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val directoryData = MutableLiveData<AllDirectoryResponseModel.Data>()

    fun fetchDirectory(input: String = "") {
        executeDirectoryAll()
            .subscribeBy(
                onNext = {
                    if (input.isNotBlank()) {
                        searchResult(input, it.data)
                    } else {
                        if (it.status)
                            directoryData.postValue(it.data)
                    }
                },
                onError = {})
            .addTo(compositeDisposable)
    }

    private fun searchResult(input: String, data: AllDirectoryResponseModel.Data?) {
        val contact = data?.contact_directories
        val searchList = contact?.filter {
            it.profile_fullname?.contains(input, true) == true
        }?.toMutableList()
        searchList?.let { directoryData.postValue(AllDirectoryResponseModel.Data(searchList)) }
    }

    fun directoryResult(): LiveData<AllDirectoryResponseModel.Data> = directoryData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
