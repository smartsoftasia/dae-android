package pattararittigul.sasin.unesco_android.view.main.community

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.extension.executeAllThread
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

class MainCommunityViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val communityData = MutableLiveData<CommunityStoryResponseModel.Data>()

    private fun searchResult(input: String, data: CommunityStoryResponseModel.Data?) {
        val stories = data?.communication_stories
        val searchList = stories?.filter { communicationStory ->
            communicationStory.title?.contains(
                input,
                true
            ) == true
        }
        communityData.postValue(searchList?.toMutableList()?.let {
            CommunityStoryResponseModel.Data(it)
        })
    }

    fun feedThread(input: String = "") {
        executeAllThread().subscribeBy(
            onNext = {
                if (input.isNotBlank()) {
                    searchResult(input, it.data)
                } else {
                    communityData.postValue(it.data)
                }
            },
            onError = {
                communityData.postValue(null)
            }
        ).addTo(compositeDisposable)
    }

    fun threadResult(): LiveData<CommunityStoryResponseModel.Data> = communityData

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
