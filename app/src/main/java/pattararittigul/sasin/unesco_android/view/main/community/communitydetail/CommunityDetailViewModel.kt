package pattararittigul.sasin.unesco_android.view.main.community.communitydetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.*
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel

class CommunityDetailViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    var storyDisplayedId: String? = ""
    private var storyData = MutableLiveData<CommunityStoryResponseModel.Data.CommunicationStory>()


    private fun fetch() {
        executeFetchDetail("$storyDisplayedId")
            .subscribeBy(
                onNext = { storyData.postValue(it.data?.communication_stories?.get(0)) },
                onError = {
                    storyData.postValue(null)
                })
            .addTo(compositeDisposable)
    }

    fun handleOnCheckBoxChange(isLike: Boolean) {
        storyDisplayedId?.let {
            executeAddLike(it, SharedPreferences.getUserDetail().id.toString(), isLike)
                .subscribeBy(
                    onNext = {
                        fetch()
                    },
                    onError = {})
                .addTo(compositeDisposable)
        }
    }

    fun submitComment(inputComment: String) {
        Log.d(
            TAG,
            "story_id: $storyDisplayedId, member_id: ${SharedPreferences.getUserDetail().id}, input_comment: $inputComment"
        )
        executeComment("$storyDisplayedId", "${SharedPreferences.getUserDetail().id}", inputComment)
            .subscribeBy(
                onNext = {
                    fetch()
                },
                onError = {}
            ).addTo(compositeDisposable)
    }

    fun storyResult(): LiveData<CommunityStoryResponseModel.Data.CommunicationStory> = storyData


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun threadViews(threadId: String){
        executeViewThread(threadId).
            subscribeBy({
                Log.d(TAG,"")
            },{}).addTo(compositeDisposable)
    }
}
