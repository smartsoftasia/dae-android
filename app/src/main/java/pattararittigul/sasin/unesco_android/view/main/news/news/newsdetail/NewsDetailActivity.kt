package pattararittigul.sasin.unesco_android.view.main.news.news.newsdetail

import android.os.Bundle
import androidx.core.content.ContextCompat
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.setStatusBarColor
import pattararittigul.sasin.unesco_android.model.NewsResponseModel
import pattararittigul.sasin.unesco_android.view.BaseActivity

class NewsDetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_detail_activity)
        setStatusBarColor(ContextCompat.getColor(this, R.color.color_primary_variant))
        if (savedInstanceState == null) {
            (intent.extras?.getParcelable("NEWS_DETAIL") as? NewsResponseModel.Data.NewsStory)?.let {
                supportFragmentManager.beginTransaction()
                    .replace(
                        R.id.container,
                        NewsDetailFragment.newInstance(
                            it
                        )
                    )
                    .commitNow()
            }
        }
    }

}
