package pattararittigul.sasin.unesco_android.view.main.news

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import pattararittigul.sasin.unesco_android.view.main.news.calendar.CalendarFragment
import pattararittigul.sasin.unesco_android.view.main.news.news.NewsFeedFragment

class NewsPagerAdapter(
    fragmentManager: FragmentManager,
    private val tabNames: MutableList<String>
) : FragmentStatePagerAdapter(fragmentManager) {
    override

    fun getItem(position: Int): Fragment {
        return if (position == 0) NewsFeedFragment.newInstance()
        else CalendarFragment.newInstance()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabNames[position]
    }

    override fun getCount(): Int = 2

    fun updateTabs(names: MutableList<String>) {
        tabNames.clear()
        tabNames.addAll(names)
        notifyDataSetChanged()
    }
}