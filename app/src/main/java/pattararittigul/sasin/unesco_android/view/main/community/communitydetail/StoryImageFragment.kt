package pattararittigul.sasin.unesco_android.view.main.community.communitydetail


import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_story_image.view.*
import pattararittigul.sasin.unesco_android.GlideApp
import pattararittigul.sasin.unesco_android.R

/**
 * A simple [Fragment] subclass.
 */
class StoryImageFragment : Fragment() {
    private lateinit var mView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mView = inflater.inflate(R.layout.fragment_story_image, container, false)
        val imageUri = Uri.parse(arguments?.getString("IMAGE_URL"))
        init(imageUri)
        return mView
    }

    private fun init(image: Uri) {
        GlideApp.with(requireContext())
            .load(image)
            .placeholder(R.drawable.place_holder_image_view)
            .error(R.drawable.place_holder_image_view)
            .centerCrop()
            .into(mView.image)
    }

    companion object {
        fun newInstance(imageUrl: String): StoryImageFragment {
            val fragment = StoryImageFragment()
            val args = Bundle()
            args.putString("IMAGE_URL", imageUrl)
            fragment.arguments = args
            return fragment
        }
    }
}
