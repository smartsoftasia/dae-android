package pattararittigul.sasin.unesco_android.view.main.library

import android.os.Environment
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.kittinunf.fuel.Fuel
import io.reactivex.disposables.CompositeDisposable
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.createFolder
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel
import java.io.File
import java.io.FileOutputStream
import java.util.*

class LibraryCategoryViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val downloadData = MutableLiveData<String>()
    var holdingBook: LibraryAllResponseModel.Data.Category.Book? = null


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun downloadFile(url: String, fileName: String) {
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (url.isNotBlank()) {
            download(url, fileName, extension)
        } else {
            downloadData.postValue(null)
        }
    }


    private fun download(url: String, fileName: String, type: String) {
        Fuel.download(url).destination { _, _ ->
            val file = File.createTempFile(UUID.randomUUID().toString(), type)
            file
        }.response { _, _, (byteArray, _) ->
            byteArray?.let {
                saveToExternalStorage(it, fileName, type)
            }.also {
                Log.d(TAG, "save response: $it")
                downloadData.postValue(it)
            }
        }
    }

    fun downloadResult(): LiveData<String> = downloadData

    private fun saveToExternalStorage(byteArray: ByteArray, name: String, type: String): String? {
        val contentFilePath = getContentFilePath(name, type)
        if (!File(contentFilePath).exists()) {
            createFolder(CONTENT_FOLDER_PATH)
        }
        return try {
            val outputStream = FileOutputStream(contentFilePath)
            outputStream.write(byteArray)
            outputStream.flush()
            outputStream.close()
            contentFilePath
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    private fun getContentFilePath(name: String, type: String): String {
        return "${Environment.getExternalStorageDirectory().absolutePath}$CONTENT_FOLDER_PATH/$name.$type"
    }

    companion object {
        private const val CONTENT_FOLDER_PATH = "/download_library"
    }

}