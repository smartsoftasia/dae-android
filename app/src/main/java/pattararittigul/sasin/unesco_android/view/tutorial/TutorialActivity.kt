package pattararittigul.sasin.unesco_android.view.tutorial

import android.os.Bundle
import androidx.core.content.ContextCompat
import pattararittigul.sasin.unesco_android.R
import pattararittigul.sasin.unesco_android.extension.setStatusBarColor
import pattararittigul.sasin.unesco_android.view.BaseActivity

class TutorialActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(ContextCompat.getColor(this, R.color.color_primary_variant))
        setContentView(R.layout.tutorial_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, TutorialFragment.newInstance())
                .commitNow()
        }
    }

    override fun onBackPressed() {
        // not allow.
    }

}
