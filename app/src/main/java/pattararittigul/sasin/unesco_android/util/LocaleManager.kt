package pattararittigul.sasin.unesco_android.util

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import androidx.annotation.StringDef
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.database.SharedPreferences.Companion.setLanguagePref
import java.util.*


class LocaleManager {

    @Retention(AnnotationRetention.SOURCE)
    @StringDef(ENGLISH, MYANMAR)
    annotation class LocaleDef {
        companion object {
            val SUPPORTED_LOCALES = arrayOf(ENGLISH, MYANMAR)
        }
    }

    companion object {
        const val ENGLISH: String = "en"
        const val MYANMAR: String = "my"

        //        fun updateResources(context: Context, language: String): Context {
//            val locale = Locale(language)
//            Locale.setDefault(locale)
//            val config = Configuration(context.resources.configuration)
//            config.setLocale(locale)
//            context = context.createConfigurationContext(config)
//        }
        private fun updateResources(context: Context, language: String): Context {
            var mContext = context
            val locale = Locale(language)
            Locale.setDefault(locale)

            val res = mContext.resources
            val config = Configuration(res.configuration)
            config.setLocale(locale)
            mContext = mContext.createConfigurationContext(config)
            return mContext
        }

        fun setCurrentLocale(context: Context): Context {
            return updateResources(context, SharedPreferences.getLanguagePref())
        }


        fun updateToLocale(context: Context, @LocaleDef language: String): Context {
            setLanguagePref(language)
            return updateResources(context, language)
        }

        fun getCurrentLocale(context: Context): Locale {
            val config = context.resources.configuration
            return if (Build.VERSION.SDK_INT >= 24) config.locales.get(0) else config.locale
        }
    }
}

