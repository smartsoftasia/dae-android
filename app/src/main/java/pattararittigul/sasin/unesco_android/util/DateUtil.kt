package pattararittigul.sasin.unesco_android.util

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {
        @JvmStatic
        private val appTimeZone by lazy {
            return@lazy TimeZone.getTimeZone("Thailand/Bangkok")
        }

        @JvmStatic
        fun formatDateStringToDate(date: String, datePattern: String): DateTime {
            val format = DateTimeFormat.forPattern(datePattern)
            return format.parseDateTime(date)
        }

        @JvmStatic
        fun formatDateMillisToDateString(millis: Long, datePattern: String): String? {
            val formatter = SimpleDateFormat(datePattern, Locale.US)
            return formatter.format(millis)
        }

    }


}