package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class BaseResponse(
    var message: String = "",
    var status: Boolean = false
) : Parcelable