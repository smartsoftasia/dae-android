package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LibraryAllResponseModel(
    var data: Data? = Data()
) : BaseResult(), Parcelable {
    @Parcelize
    data class Data(
        var categories: MutableList<Category>? = mutableListOf()
    ) : Parcelable {
        @Parcelize
        data class Category(
            var books: MutableList<Book>? = mutableListOf(),
            var category_id: Int? = 0,
            var category_name: String? = ""
        ) : Parcelable {
            @Parcelize
            data class Book(
                var book_id: Int? = 0,
                var cover_image: String? = "",
                var date_created: Long? = 0,
                var description: String? = "",
                var download_url: String? = "",
                var title: String? = "",
                var author_name: String? = ""
            ) : Parcelable
        }
    }
}