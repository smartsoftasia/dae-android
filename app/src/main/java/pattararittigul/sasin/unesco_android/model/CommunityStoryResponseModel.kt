package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CommunityStoryResponseModel(
    var data: Data? = Data()
) : BaseResult(), Parcelable {
    @Parcelize
    data class Data(
        var communication_stories: MutableList<CommunicationStory> = mutableListOf()
    ) : Parcelable {
        @Parcelize
        data class CommunicationStory(
            var comments: MutableList<Comment>? = mutableListOf(),
            var creator_user_id: String? = "", // 12
            var date_created: Long? = 0L, // 1571816904
            var description_content: String? = "", // about content bla bla bla..
            var images: MutableList<String>? = mutableListOf(),
            var is_recommend: Boolean? = false, // true
            var is_visible: Boolean? = false, // true
            var story_id: String? = "", // 0
            var title: String? = "", // The title of story
            var total_viewed_amount: Int? = 0, // 150,
            var count_like: Int? = 0,
            var is_liked: Boolean? = false
        ) : Parcelable {
            @Parcelize
            data class Comment(
                var comment_id: Int? = 0,
                var comment_content: String? = "", // this story is very interesting.
                var date_created: Long? = 0L, // 1571817084
                var is_visible: Boolean? = false, // true
                var writer_image: String? = "",
                var writer_name: String? = ""// 15
            ) : Parcelable
        }
    }
}