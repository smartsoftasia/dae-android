package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationAllResponseModel(
    var data: Data? = Data()
) : BaseResult(), Parcelable {
    @Parcelize
    data class Data(
        var notifications: MutableList<NotificationModel>? = mutableListOf()
    ) : Parcelable
}