package pattararittigul.sasin.unesco_android.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import pattararittigul.sasin.unesco_android.enum.TutorialItemStyle

data class TutorialItemModel(
    @DrawableRes val image: Int, @StringRes val header: Int, @StringRes val description: Int, val style: TutorialItemStyle
)
