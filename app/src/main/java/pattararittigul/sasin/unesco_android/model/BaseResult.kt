package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseResult : Parcelable {
    @IgnoredOnParcel
    internal var status: Boolean = false
    @IgnoredOnParcel
    internal var message: String = ""
}