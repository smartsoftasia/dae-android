package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProfileModel(
    var data: Data? = Data()
) : BaseResult(), Parcelable {
    @Parcelize
    data class Data(
        var first_name: String? = "",
        var howetown: String? = "",
        var id: String? = "",
        var last_name: String? = "",
        var profile: String? = "",
        var count_notification: Int? = 0
    ) : Parcelable
}