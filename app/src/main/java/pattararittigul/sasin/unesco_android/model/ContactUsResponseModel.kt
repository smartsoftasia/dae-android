package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactUsResponseModel(
    var data: Data? = Data()
) : BaseResult(), Parcelable {
    @Parcelize
    data class Data(
        var email: Email? = Email(),
        var phone: Phone? = Phone(),
        var website: Website? = Website()
    ) : Parcelable {
        @Parcelize
        data class Email(
            var email_address: String? = "",
            var email_display: String? = ""
        ) : Parcelable

        @Parcelize
        data class Phone(
            var phone_display: String? = "",
            var phone_number: String? = ""
        ) : Parcelable

        @Parcelize
        data class Website(
            var website_display: String? = "",
            var website_url: String? = ""
        ) : Parcelable
    }
}