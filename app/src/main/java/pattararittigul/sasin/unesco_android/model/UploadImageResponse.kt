package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UploadImageResponse(
    var data: String? = ""
) : BaseResult(), Parcelable