package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventCalendarResponseModel(
    var data: Data? = Data()
) : BaseResult(), Parcelable {
    @Parcelize
    data class Data(
        var calendar_months: MutableList<CalendarMonth>? = mutableListOf()
    ) : Parcelable {
        @Parcelize
        data class CalendarMonth(
            var day_in_month: Int? = 0, // 30
            var days: MutableList<Day>? = mutableListOf(),
            var month_id: String? = "", // 1
            var month_name: String? = "", // November
            var month_of_year: Int? = 0, // 10
            var year: Int? = 0 // 2019
        ) : Parcelable {
            @Parcelize
            data class Day(
                var date: String? = "", // 3
                var day_of_month: Int? = 0, // 7
                var events: MutableList<Event>? = mutableListOf(),
                var is_today: Boolean? = false // false
            ) : Parcelable {
                @Parcelize
                data class Event(
                    var event_description: String? = "",
                    var event_end: Long? = 0, // 0
                    var event_id: Int? = 0, // 0
                    var event_name: String? = "",
                    var event_start: Long? = 0 // 0
                ) : Parcelable
            }
        }
    }
}