package pattararittigul.sasin.unesco_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllDirectoryResponseModel(
    var data: Data? = Data()
) : BaseResult(), Parcelable {
    @Parcelize
    data class Data(
        var contact_directories: MutableList<ContactDirectory>? = mutableListOf()
    ) : Parcelable {
        @Parcelize
        data class ContactDirectory(
            var profile_fullname: String? = "",
            var profile_image: String? = "",
            var profile_organization: String? = "",
            var profile_phone: String? = "",
            var profile_email: String? = "",
            var profile_township: String? = ""
        ) : Parcelable
    }
}