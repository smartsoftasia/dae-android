package pattararittigul.sasin.unesco_android

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.facebook.AccessToken
import com.facebook.Profile
import pattararittigul.sasin.unesco_android.database.SharedPreferences
import pattararittigul.sasin.unesco_android.extension.TAG
import pattararittigul.sasin.unesco_android.extension.toActivity
import pattararittigul.sasin.unesco_android.view.login.LoginActivity
import pattararittigul.sasin.unesco_android.view.main.MainActivity

class SchemeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val uri = intent?.data
        val uriString = uri?.toString()
        Log.d(TAG, "deepLink: $uriString")
        facebookLoginChecked()
        toNextActivity()
    }

    private fun toNextActivity() {
        if (SharedPreferences.isLogin()) toActivity<MainActivity>()
        else toActivity<LoginActivity>()
        finishAffinity()
    }

    private fun facebookLoginChecked() {
        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedInWithFacebook = accessToken != null && !accessToken.isExpired
        if (isLoggedInWithFacebook) {
//            SharedPreferences.setLogin(true)
            val facebookProfile: Profile = Profile.getCurrentProfile()
            Log.d(TAG, "icon_fb_: ${facebookProfile.id}")
            toActivity<MainActivity>()
        } else {
            toActivity<LoginActivity>()
        }
    }

}