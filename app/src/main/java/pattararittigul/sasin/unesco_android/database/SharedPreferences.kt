package pattararittigul.sasin.unesco_android.database

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import pattararittigul.sasin.unesco_android.model.ProfileModel
import pattararittigul.sasin.unesco_android.util.LocaleManager

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "UNUSED")
class SharedPreferences {

    companion object {
        private const val prefLogin = "PREF_LOGIN"
        private const val prefLanguage = "PREF_LANGUAGE"
        private const val prefLanguageId = "PREF_LANGUAGE_ID"
        private const val prefFirstTimeUsed = "PREF_FIRST_TIME_USE"
        private const val prefUserDetail = "PREF_USER_DETAIL"


        private fun setLanguageId(languageId: Int) {
            putInt(prefLanguageId, languageId)
        }

        fun getLanguageId() = getInt(prefLanguageId, 1)

        fun setLanguagePref(localeKey: String) {
            setLanguageId(
                if (localeKey == LocaleManager.ENGLISH) 1
                else 2
            )
            putString(prefLanguage, localeKey)
        }

        fun getLanguagePref(): String {
            return getString(prefLanguage, LocaleManager.ENGLISH)
        }

        fun setUserDetail(userDetail: ProfileModel.Data) {
            val userDetailJson: String = Gson().toJson(userDetail)
            putString(prefUserDetail, userDetailJson)
        }

        fun getUserDetail(): ProfileModel.Data {
            val userDetailJson = getString(prefUserDetail)
            return if (userDetailJson.isNotBlank()) {
                Gson().fromJson(userDetailJson, ProfileModel.Data::class.java)
            } else {
                ProfileModel.Data()
            }
        }

        fun wasDisplayTutorial() = getBoolean(prefFirstTimeUsed)

        fun setDisplayedTutorial(isUsed: Boolean) {
            putBoolean(prefFirstTimeUsed, isUsed)
        }

        fun isLogin() =
            getBoolean(prefLogin)

        fun setLogin(isLogin: Boolean) {
            putBoolean(prefLogin, isLogin)
        }

        private fun getInt(key: String, defaultValue: Int = 0): Int {
            return preferences().getInt(key, defaultValue)
        }

        private fun putInt(key: String, value: Int) {
            prefEdit().putInt(key, value).apply()
        }

        private fun getLong(key: String): Long {
            return preferences().getLong(key, 0L)
        }

        private fun putLong(key: String, value: Long) {
            prefEdit().putLong(key, value).apply()
        }

        private fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
            return preferences().getBoolean(
                key, defaultValue
            )
        }

        private fun putBoolean(key: String, value: Boolean) {
            prefEdit().putBoolean(key, value).apply()
        }

        private fun getString(key: String, defaultValue: String = ""): String {
            return preferences().getString(key, defaultValue)?:""
        }

        private fun putString(key: String, value: String) {
            prefEdit().putString(key, value).apply()
        }

        fun clearPreference() {
            prefEdit().clear().apply()
        }

        private fun removePreference(key: String) {
            prefEdit().remove(key).apply()
        }


        @JvmStatic
        @Volatile
        private var applicationContext: Context? = null

        @JvmStatic
        @Synchronized
        fun init(context: Context?) {
            requireNotNull(context) { "Non-null context required." }
            applicationContext = context.applicationContext
        }

        @JvmStatic
        fun getModuleContext(): Context {
            return checkNotNull(applicationContext)
        }

        private fun preferences(): SharedPreferences {
            return getModuleContext().getSharedPreferences(
                "unesco_preference",
                Context.MODE_PRIVATE
            )
        }

        private fun prefEdit(): SharedPreferences.Editor {
            return preferences().edit()
        }
    }
}