package pattararittigul.sasin.unesco_android

import android.content.Context
import android.content.res.Configuration
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate
import com.facebook.appevents.AppEventsLogger


class App : MultiDexApplication() {

    var localizationDelegate = LocalizationApplicationDelegate(this)
    override fun onCreate() {
        super.onCreate()
        AppEventsLogger.activateApp(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base))
        MultiDex.install(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        localizationDelegate.onConfigurationChanged(this)
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())

    }

}