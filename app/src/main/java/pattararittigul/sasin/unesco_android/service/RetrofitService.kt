package pattararittigul.sasin.unesco_android.service

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pattararittigul.sasin.unesco_android.BuildConfig
import pattararittigul.sasin.unesco_android.constant.API_KEY
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitService {

    companion object {
        fun retrofit(): Retrofit {
            val url = BuildConfig.SERVER_URL
            val gSon = GsonBuilder()
                .setLenient()
                .create()
            return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .client(getOkHttpClient())
                .baseUrl(url)
                .build()
        }

        @JvmStatic
        private fun getOkHttpClient(): OkHttpClient {
            val bodyLog = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            val client = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor(bodyLog)
                .addInterceptor {
                    val request = (
                            it.request()
                                .newBuilder()
                                .addHeader("X-Api-Key", API_KEY)
                                .addHeader(
                                    "Content-Type",
                                    "application/x-www-form-urlencoded"
                                ).build())
                    it.proceed(request)
                }
            return client.build()
        }
    }
}