package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.RequestBody
import pattararittigul.sasin.unesco_android.model.BaseResponse
import pattararittigul.sasin.unesco_android.model.CommunityStoryResponseModel
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ThreadApi {
    @Multipart
    @POST("/api/thread/all")
    fun getThreadAll(@Part("member_id") memberId: RequestBody): Observable<CommunityStoryResponseModel>

    @Multipart
    @POST("api/thread/add")
    fun addStory(
        @Part("member_id") memberId: RequestBody,
        @Part("image") images: RequestBody?,
        @Part("title") title: RequestBody,
        @Part("detail") detail: RequestBody
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/coment/add")
    fun addComment(
        @Part("id_thread_comment") storyId: RequestBody,
        @Part("member_id") memberId: RequestBody,
        @Part("comment") comment: RequestBody
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/thread/thread_by_id")
    fun fetchDetail(@Part("id_thread") storyId: RequestBody, @Part("member_id") memberId: RequestBody): Observable<CommunityStoryResponseModel>

    @Multipart
    @POST("api/thread_likes/add")
    fun addLike(
        @Part("id_thread") storyId: RequestBody,
        @Part("member_id") memberId: RequestBody,
        @Part("like") isLike: Boolean
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/thread/view")
    fun threadView(@Part("id_thread") idThread: RequestBody): Observable<Response<Void>>
}