package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.RequestBody
import pattararittigul.sasin.unesco_android.model.EventCalendarResponseModel
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface EventApi {

    @Multipart
    @POST("api/event/all")
    fun getEventAll(@Part("language_id") languageId: RequestBody): Observable<EventCalendarResponseModel>


}