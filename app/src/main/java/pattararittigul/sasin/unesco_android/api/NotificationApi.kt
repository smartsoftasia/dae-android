package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.RequestBody
import pattararittigul.sasin.unesco_android.model.NotificationAllResponseModel
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface NotificationApi {

    @Multipart
    @POST("api/user_notifications/all")
    fun getNotificationAll(@Part("member_id") memberId: RequestBody, @Part("language_id") languageId: RequestBody): Observable<NotificationAllResponseModel>

}