package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.RequestBody
import pattararittigul.sasin.unesco_android.model.LibraryAllResponseModel
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface LibraryApi {

    @Multipart
    @POST("api/ilbrary/all")
    fun getLibraryAll(
        @Part("language_id") languageId: RequestBody,
        @Part("file_category") fileCategory: RequestBody
    ): Observable<LibraryAllResponseModel>
}