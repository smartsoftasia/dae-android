package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.MultipartBody
import pattararittigul.sasin.unesco_android.model.UploadImageResponse
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface UploadApi {

    @Multipart
    @POST("api/uploadimage/add")
    fun uploadImage(@Part image: MultipartBody.Part): Observable<UploadImageResponse>
}