package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pattararittigul.sasin.unesco_android.model.AboutUsResponseModel
import pattararittigul.sasin.unesco_android.model.AllDirectoryResponseModel
import pattararittigul.sasin.unesco_android.model.BaseResponse
import pattararittigul.sasin.unesco_android.model.ContactUsResponseModel
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface MenuApi {

    @Multipart
    @POST("api/aboutus/all")
    fun aboutusAll(@Part("language_id") languageId: RequestBody): Observable<AboutUsResponseModel>

    @Multipart
    @POST("api/contactus/all")
    fun contactusAll(@Part("language_id") languageId: RequestBody): Observable<ContactUsResponseModel>

    @Multipart
    @POST("api/directory_members/all")
    fun directoryAll(@Part("language_id") languageId: RequestBody, @Part("member_id") id: RequestBody): Observable<AllDirectoryResponseModel>

    @Multipart
    @POST("api/directory_members/add")
    fun addDirectory(
        @Part("member_id") memberId: RequestBody,
        @Part profile: MultipartBody.Part?,
        @Part("profile_fullname") firstName: RequestBody?,
        @Part("profile_organization") lastName: RequestBody?,
        @Part("profile_phone") phone: RequestBody?,
        @Part("profile_email") email: RequestBody?,
        @Part("profile_township") township: RequestBody?
    ): Observable<BaseResponse>
}