package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pattararittigul.sasin.unesco_android.model.BaseResponse
import pattararittigul.sasin.unesco_android.model.ProfileModel
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface MemberApi {

    @Multipart
    @POST("api/member/add")
    fun registerAccount(
        @Part("first_name") firstName: RequestBody,
        @Part("last_name") lastName: RequestBody,
        @Part("howetown") hometown: RequestBody?,
        @Part("email") email: RequestBody,
        @Part("password") password: RequestBody
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/member/login")
    fun loginAccount(
        @Part("email") email: RequestBody,
        @Part("password") password: RequestBody
    ): Observable<ProfileModel>

    @Multipart
    @POST("api/member/update")
    fun updateAccount(
        @Part("first_name") firstName: RequestBody?,
        @Part("last_name") lastName: RequestBody?,
        @Part("howetown") hometown: RequestBody?,
        @Part profile: MultipartBody.Part?,
        @Part("id") id: RequestBody,
        @Part("app_token") appToken: RequestBody?
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/member/detail")
    fun getAccountDetail(@Part("id") id: RequestBody): Observable<ProfileModel>

    @Multipart
    @POST("api/member/facebooklogin")
    fun loginFacebook(@Part("login") login: RequestBody, @Part("token") token: RequestBody): Observable<ProfileModel>

    @Multipart
    @POST("api/member/resetpassword")
    fun resetPassword(@Part("email") email: RequestBody): Observable<BaseResponse>

}