package pattararittigul.sasin.unesco_android.api

import io.reactivex.Observable
import okhttp3.RequestBody
import pattararittigul.sasin.unesco_android.model.NewsResponseModel
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface NewsApi {
    @Multipart
    @POST("api/news/all")
    fun getAllNews(@Part("language_id") languageId: RequestBody): Observable<NewsResponseModel>

    @Multipart
    @POST("api/news/view")
    fun newsView(@Part("id_news") idNews: RequestBody): Observable<Response<Void>>

}