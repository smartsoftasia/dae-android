package pattararittigul.sasin.unesco_android.enum

enum class MainNavigationMenu(val tabPosition: Int) {
    COMMUNITY(0), LIBRARY(1), NEWS_AND_EVENT(2), NOTIFICATION(3), MENU(4)
}