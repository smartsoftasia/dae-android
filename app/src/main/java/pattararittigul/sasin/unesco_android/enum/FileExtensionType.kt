package pattararittigul.sasin.unesco_android.enum

class FileExtensionType {

    companion object {
        val dataTypes: MutableList<Pair<MutableList<String>, String>> =
            mutableListOf(
                Pair(mutableListOf(".doc", ".docx"), "application/msword"),
                Pair(mutableListOf(".pdf"), "application/pdf"),
                Pair(mutableListOf(".ppt", ".pttx"), "application/vnd.ms-powerpoint"),
                Pair(mutableListOf(".xls", ".xlsx"), "application/vnd.ms-excel"),
                Pair(mutableListOf(".rtf"), "application/rtf"),
                Pair(mutableListOf(".zip", ".rar"), "application/x-wav"),
                Pair(mutableListOf(".wav", ".mp3"), "audio/x-wav"),
                Pair(mutableListOf(".gif"), "image/gif"),
                Pair(mutableListOf(".txt"), "text/plain"),
                Pair(mutableListOf(".jpg", ".jpeg", ".png"), "image /jpg"),
                Pair(mutableListOf(".3gp", ".mpg", ".mpeg", ".mpe", ".mp4", ".avi"), "video/*")
            )
    }
}
