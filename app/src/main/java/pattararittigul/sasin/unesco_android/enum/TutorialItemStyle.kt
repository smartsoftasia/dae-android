package pattararittigul.sasin.unesco_android.enum

enum class TutorialItemStyle(val type: Int) {
    LEFT(0), RIGHT(1)
}