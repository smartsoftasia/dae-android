package pattararittigul.sasin.unesco_android.enum

enum class DateTimeFormat(val pattern: String) {
    DATE_DEFAULT("dd MMM yyyy"), TIME_DEFAULT("hh:mm aa"), DATE_TIME_SHORT_DEFAULT("d/MM/yy hh:mm aa")
}